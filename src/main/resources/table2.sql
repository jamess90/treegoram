
insert into user_list 
values(1,'h591264@naver.com','1234','박지우',01050179802,441390,
'dfdf','dfdf','2014-04-02','sfds',0,'')
drop TRIGGER user_list_user_no_AI_TRG;
drop TRiGGER timeline_timeline_no_AI_TRG;
insert into PRODUCT_PHOTO values(1,1,'dsdsd');
select TRiGGER_Name from user_Triggers;

select t.timeline_no, t.timeline_title, t.timeline_content, t.user_no, t.reg_date,
p.timeline_photo_file from TIMELINE t, TIMELINE_PHOTO p
where t.timeline_no = p.timeline_no(+)
order by t.timeline_no desc

select * from timeline_photo;

select timeline_no, timeline_title, TIMELINE_content, user_no, reg_date 
from(
	select ceil(rownum/3) page,  timeline_no, timeline_title, TIMELINE_content, user_no, reg_date 
	from(
		select timeline_no, timeline_title, TIMELINE_content, user_no, reg_date from timeline order by timeline_no desc  
	) timeline, (select timeline_no, timeline_photo_file from timeline_photo) timeline_photo
)
where page =1

select t.timeline_no, t.timeline_title, t.timeline_content, t.user_no, t.reg_date,
p.timeline_photo_file from TIMELINE t, TIMELINE_PHOTO p
where t.timeline_no = p.timeline_no(+)
order by t.timeline_no desc


-- user_list Table Create SQL
CREATE TABLE user_list
(
    user_no             NUMBER           NOT NULL, 
    email               VARCHAR2(50)     NOT NULL, 
    password            VARCHAR2(50)     NOT NULL, 
    user_name           VARCHAR2(20)     NOT NULL, 
    phone               NUMBER           NULL, 
    user_post_number    NUMBER           NULL, 
    user_address1       VARCHAR2(100)    NULL, 
    user_address2       VARCHAR2(100)    NULL, 
    birthday            DATE             NULL, 
    user_photo_file     VARCHAR2(100)    NULL, 
    penalty             NUMBER(1)        DEFAULT 0, 
    reg_date            DATE             DEFAULT sysdate, 
    CONSTRAINT USER_LIST_PK PRIMARY KEY (user_no)
);


CREATE SEQUENCE user_list_SEQ
START WITH 1
INCREMENT BY 1;


-- product Table Create SQL
CREATE TABLE product
(
    product_no       NUMBER            NOT NULL, 
    product_name     VARCHAR2(50)      NOT NULL, 
    category         VARCHAR2(50)      NOT NULL, 
    start_price      NUMBER            NOT NULL, 
    raise_unit       NUMBER            NOT NULL, 
    current_price    NUMBER            NOT NULL, 
    text             VARCHAR2(1000)    NULL, 
    like_number      NUMBER            DEFAULT 0, 
    prompt_price     NUMBER            NOT NULL, 
    user_no          NUMBER            NOT NULL, 
    start_time       TIMESTAMP         DEFAULT sysdate, 
    end_time         TIMESTAMP         NOT NULL, 
    CONSTRAINT PRODUCT_PK PRIMARY KEY (product_no)
);


CREATE SEQUENCE product_SEQ
START WITH 1
INCREMENT BY 1;


ALTER TABLE product
    ADD CONSTRAINT FK_product_user_no_user_list_u FOREIGN KEY (user_no)
        REFERENCES user_list (user_no) on delete cascade;



-- timeline Table Create SQL
CREATE TABLE timeline
(
    timeline_no         NUMBER            NOT NULL, 
    timeline_title      VARCHAR2(100)     NOT NULL, 
    timeline_content    VARCHAR2(1000)    NOT NULL, 
    user_no             NUMBER            NOT NULL, 
    reg_date            DATE              DEFAULT sysdate, 
    CONSTRAINT TIMELINE_PK PRIMARY KEY (timeline_no)
);


CREATE SEQUENCE timeline_SEQ
START WITH 1
INCREMENT BY 1;

ALTER TABLE timeline
    ADD CONSTRAINT FK_timeline_user_no_user_list_ FOREIGN KEY (user_no)
        REFERENCES user_list (user_no) on delete cascade;

        
-- timeline_photo Table Create SQL
CREATE TABLE timeline_photo
(
    timeline_no            NUMBER           NOT NULL, 
    timeline_photo_file    VARCHAR2(100)    NULL   
);


ALTER TABLE timeline_photo
    ADD CONSTRAINT FK_timeline_photo_timeline_no_ FOREIGN KEY (timeline_no)
        REFERENCES timeline (timeline_no) on delete cascade;        

        
-- chating Table Create SQL
CREATE TABLE chating
(
    chat_no         NUMBER    NOT NULL, 
    from_user_no    NUMBER    NOT NULL, 
    to_user_no      NUMBER    NOT NULL, 
    CONSTRAINT CHATING_PK PRIMARY KEY (chat_no)
);


CREATE SEQUENCE chating_SEQ
START WITH 1
INCREMENT BY 1;


ALTER TABLE chating
    ADD CONSTRAINT FK_chating_from_user_no_user_l FOREIGN KEY (from_user_no)
        REFERENCES user_list (user_no);


ALTER TABLE chating
    ADD CONSTRAINT FK_chating_to_user_no_user_lis FOREIGN KEY (to_user_no)
        REFERENCES user_list (user_no);



-- product_photo Table Create SQL
CREATE TABLE product_photo
(
    product_no            NUMBER          NOT NULL, 
    product_photo_file    VARCHAR2(50)    NULL
);



CREATE SEQUENCE product_photo_SEQ
START WITH 1
INCREMENT BY 1;



ALTER TABLE product_photo
    ADD CONSTRAINT FK_product_photo_product_no_pr FOREIGN KEY (product_no)
        REFERENCES product (product_no) on delete cascade;



-- product_comment Table Create SQL
CREATE TABLE product_comment
(
    comment_no    NUMBER            NOT NULL, 
    product_no    NUMBER            NOT NULL, 
    user_no       NUMBER            NOT NULL, 
    text          VARCHAR2(1000)    NULL, 
    reg_date      TIMESTAMP         DEFAULT sysdate, 
    CONSTRAINT PRODUCT_COMMENT_PK PRIMARY KEY (comment_no)
);


CREATE SEQUENCE product_comment_SEQ
START WITH 1
INCREMENT BY 1;


ALTER TABLE product_comment
    ADD CONSTRAINT FK_product_comment_product_no_ FOREIGN KEY (product_no)
        REFERENCES product (product_no) on delete cascade;


ALTER TABLE product_comment
    ADD CONSTRAINT FK_product_comment_user_no_use FOREIGN KEY (user_no)
        REFERENCES user_list (user_no) on delete cascade;



-- order_list Table Create SQL
CREATE TABLE order_list
(
    order_no             NUMBER           NOT NULL, 
    product_no           NUMBER           NOT NULL, 
    user_no              NUMBER           NOT NULL, 
    order_post_number    VARCHAR2(20)     NOT NULL, 
    order_address1       VARCHAR2(100)    NOT NULL, 
    order_address2       VARCHAR2(100)    NOT NULL, 
    price                NUMBER           NOT NULL, 
    order_date           DATE             DEFAULT sysdate, 
    CONSTRAINT ORDER_LIST_PK PRIMARY KEY (order_no)
);


CREATE SEQUENCE order_list_SEQ
START WITH 1
INCREMENT BY 1;


ALTER TABLE order_list
    ADD CONSTRAINT FK_order_list_product_no_produ FOREIGN KEY (product_no)
        REFERENCES product (product_no);


ALTER TABLE order_list
    ADD CONSTRAINT FK_order_list_user_no_user_lis FOREIGN KEY (user_no)
        REFERENCES user_list (user_no);


-- admin Table Create SQL
CREATE TABLE admin
(
    admin_no          NUMBER          NOT NULL, 
    admin_id          VARCHAR2(50)    NOT NULL, 
    admin_password    VARCHAR2(50)    NOT NULL, 
    CONSTRAINT ADMIN_PK PRIMARY KEY (admin_no)
);

CREATE SEQUENCE admin_SEQ
START WITH 1
INCREMENT BY 1;


-- friend Table Create SQL
CREATE TABLE friend
(
    user_no      NUMBER    NOT NULL, 
    friend_no    NUMBER    NOT NULL
);

insert into friend values(1,2);
select user_no from friend where friend_no=3;


ALTER TABLE friend
    ADD CONSTRAINT FK_friend_user_no_user_list_us FOREIGN KEY (user_no)
        REFERENCES user_list (user_no);

ALTER TABLE friend
    ADD CONSTRAINT FK_friend_friend_no_user_list_ FOREIGN KEY (friend_no)
        REFERENCES user_list (user_no);


-- bid_list Table Create SQL
CREATE TABLE bid_list
(
    product_no     NUMBER         NOT NULL, 
    user_no        NUMBER         NOT NULL, 
    bid_price      NUMBER         NOT NULL, 
    bid_success    VARCHAR2(1)    NULL   
);

ALTER TABLE bid_list
    ADD CONSTRAINT FK_bid_list_product_no_product FOREIGN KEY (product_no)
        REFERENCES product (product_no);


-- chating_message Table Create SQL
CREATE TABLE chating_message
(
    msg_no          NUMBER            NOT NULL, 
    chat_no         NUMBER            NOT NULL, 
    message         VARCHAR2(1000)    NULL, 
    from_user_no    NUMBER            NOT NULL, 
    time            TIMESTAMP         DEFAULT sysdate, 
    CONSTRAINT CHATING_MESSAGE_PK PRIMARY KEY (msg_no)
);

CREATE SEQUENCE chating_message_SEQ
START WITH 1
INCREMENT BY 1;


ALTER TABLE chating_message
    ADD CONSTRAINT FK_chating_message_chat_no_cha FOREIGN KEY (chat_no)
        REFERENCES chating (chat_no);

        
ALTER TABLE chating_message
    ADD CONSTRAINT FK_chating_message_from_user_n FOREIGN KEY (from_user_no)
        REFERENCES user_list (user_no);


-- timeline_comment Table Create SQL
CREATE TABLE timeline_comment
(
    timeline_comment_no    NUMBER            NOT NULL, 
    timeline_no            NUMBER            NOT NULL, 
    content                VARCHAR2(1000)    NULL, 
    user_no                number            NOT NULL, 
    time                   TIMESTAMP         DEFAULT sysdate, 
    CONSTRAINT TIMELINE_COMMENT_PK PRIMARY KEY (timeline_comment_no)
);

CREATE SEQUENCE timeline_comment_SEQ
START WITH 1
INCREMENT BY 1;



ALTER TABLE timeline_comment
    ADD CONSTRAINT FK_timeline_comment_timeline_n FOREIGN KEY (timeline_no)
        REFERENCES timeline (timeline_no) on delete cascade;


-- wish_list Table Create SQL
CREATE TABLE wish_list
(
    wish_list_no    NUMBER    NOT NULL, 
    user_no         NUMBER    NOT NULL, 
    product_no      NUMBER    NULL, 
    CONSTRAINT WISH_LIST_PK PRIMARY KEY (wish_list_no)
);


CREATE SEQUENCE wish_list_SEQ
START WITH 1
INCREMENT BY 1;


ALTER TABLE wish_list
    ADD CONSTRAINT FK_wish_list_user_no_user_list FOREIGN KEY (user_no)
        REFERENCES user_list (user_no);

ALTER TABLE wish_list
    ADD CONSTRAINT FK_wish_list_product_no_produc FOREIGN KEY (product_no)
        REFERENCES product (product_no);


