package com.treegoram.commons;


import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

public class ImageView extends AbstractView{

	private String src;
	
	/**
	 * @param src
	 * 		읽을 파일명
	 */
	
	public ImageView(String src) {
		this.src = src;
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> modelMap, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if(src == null) {
			return ;
		}
		String extention = src.substring(src.lastIndexOf(".")+1, src.length());
		if("png".equals(extention))
		{
			response.setContentType(MediaType.IMAGE_PNG_VALUE);
		}
		else if("jpg".equals(extention) || "jpeg".equals(extention)) {
			response.setContentType(MediaType.IMAGE_JPEG_VALUE);
		} else {
			return ;
		}
		FileInputStream fin = null;
		
		try {
			fin = new FileInputStream(new File(src));
			
		} catch (Exception e) {
			return ;
		}
		
		FileCopyUtils.copy(fin, response.getOutputStream());
	}
	


}
