package com.treegoram.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.treegoram.dto.ProductDTO;
import com.treegoram.service.ProductService;

@Controller
public class ProductAddController {

	@Autowired
	private ProductService service;
	
	@RequestMapping(value = "/productAddPage", method = RequestMethod.GET)
	public String productAddPage(){
		return "productAdd/productAdd_body";
	}
	
	@RequestMapping(value = "/productAdd", method = RequestMethod.POST)
	public String productAdd(@ModelAttribute ProductDTO pd, BindingResult errors,ModelMap map,  HttpServletRequest request, HttpSession session, Model model) throws IllegalStateException, IOException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String realContent = pd.getText().replaceAll("\r\n", "<br/>");
		pd.setText(realContent);
		pd.setCurrentPrice(pd.getStartPrice());
		System.out.println(pd);
		
		System.out.println(service.InsertProduct(pd)+"개의 상품이 등록되었습니다.");
		pd.setProductNo(service.selectProductNoSeq());
		System.out.println(pd);
		List files = pd.getUpfiles();
		String saveDir = "";	// ******* 밑에 saveDir 뷰단으로 경로 가져가기 위해서 뺌
		if(files!=null){//upfile이름의 요청파라미터가 있는경우.
			String defaultPath = session.getServletContext().getRealPath("/"); //서버기본경로 (프로젝트 폴더 아님) ***
			System.out.println("defaultPath: " + defaultPath);
			saveDir = defaultPath + File.separator + "uploadImg" + File.separator; // *********
		
			// 저장경로 처리
	        File directory = new File(saveDir);									// ******* 그리고 위에 파라미터 Model model 추가
	        if(!directory.exists()) { //디렉토리 존재하지 않을경우 디렉토리 생성
	            directory.mkdirs();														
	        }																	// *******
	        // 파일 저장명 처리
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss"); // *******
	        String today= formatter.format(new Date());							 // *******
			for(Object f : files){
				MultipartFile file = (MultipartFile)f; //업로드된 파일정보 하나.
				if(!file.isEmpty()){//전송된 파일이 있다면
					String fileName = file.getOriginalFilename();
					// 파일 저장명 처리
					String modifyName = today + "-" + UUID.randomUUID().toString().substring(20) + "." + fileName; //***
					System.out.println("fileName: " + fileName);
					System.out.println("modifyName: " + modifyName);
					// 수정된 파일명으로 업로드(중복방지)
					File dest = new File(saveDir, modifyName);
					System.out.println("saveDir="+saveDir);
					System.out.println(pd.getProductNo());
					//userNo는 session값으로 날라오는것을 가지고
					System.out.println(service.insertProductPhoto(pd.getProductNo(), modifyName)+"개의 사진이 추가되었습니다.");
					
					file.transferTo(dest);
					
				}
			}
		}
		// model.addAttribute("filePath", saveDir);
		return "redirect:/mainpage.do";
	}
	
}
