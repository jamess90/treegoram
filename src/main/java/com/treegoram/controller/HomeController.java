package com.treegoram.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.treegoram.commons.ImageView;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;
import com.treegoram.service.ProductService;

/**
 * Handles requests for the application home page.
 */

@Controller
public class HomeController {
	@Autowired
	SqlSessionTemplate sqlSession;
	@Autowired
	ProductService service;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String firstPage(HttpSession session) {
		 if(session.getAttribute("userNo")!=null){
			  return "redirect:/mainpage";
		  }
		return "home";
	}
	
	@RequestMapping(value = "/mainpage", method=RequestMethod.POST)
	   public String mainPage(Model model, HttpSession session) {
	      ProductDTO dto = new ProductDTO();
	      dto.setPageNo(1);
	      dto.setEndpageNo(5);
	      int userNo = (Integer) session.getAttribute("userNo");
	      List<WishListDTO> wishList = service.getWishList(userNo);
	      List<ProductDTO> mainList = service.getMainList(dto);   // 전체 상품목록 가져오기
	      List<ProductDTO> photoList = service.getPhotoList(); // 전체 상품 사진목록 가져오기'
	      List<ProdCommentDTO> commentList = service.getcommentList(); // 전체 댓글목록 가져오기
	      int maxPno = service.getMaxPno();
	      model.addAttribute("mainList", mainList);
	      model.addAttribute("photoList", photoList);
	      model.addAttribute("commentList", commentList);
	      model.addAttribute("wishList", wishList);
	      model.addAttribute("maxPno", maxPno);
	      return "main/mainpage";
	   }
	   
	   @RequestMapping(value="/mainpage", method=RequestMethod.GET)
	   public String mainPageGET(HttpServletRequest request, Model model, HttpSession session) {
		   if(session.getAttribute("userNo")==null){
				  return "redirect:/";
			  }
	      ProductDTO dto = new ProductDTO();
	      dto.setCategory(request.getParameter("category")); // 카테고리 세팅
	      dto.setPageNo(1);
	      dto.setEndpageNo(5);
	      int userNo = (Integer) session.getAttribute("userNo");
	      // List<WishListDTO> wishList = service.getWishList(userNo);
	      List<ProductDTO> mainList2 = service.getMainList(dto);   // 전체 상품목록 가져오기
	      List<ProductDTO> mainList = new ArrayList<ProductDTO>();
	      for(ProductDTO pdto : mainList2) {
	         pdto.setIsWish(service.isWish(pdto.getProductNo()));
	         mainList.add(pdto);
	      }
	      // productNo = 상품번호 
	      // SET 
	      List<ProductDTO> photoList = service.getPhotoList(); // 전체 상품 사진목록 가져오기'
	      List<ProdCommentDTO> commentList = service.getcommentList(); // 전체 댓글목록 가져오기
	      int maxPno = service.getMaxPno();
	      model.addAttribute("mainList", mainList);
	      model.addAttribute("photoList", photoList);
	      model.addAttribute("commentList", commentList);
	      model.addAttribute("maxPno", maxPno);
	      // model.addAttribute("wishList", wishList);
	      // System.out.println("wishList: " + wishList);
	      return "main/mainpage";
	   }

	
	@RequestMapping(value = "/friendProductPage", method=RequestMethod.POST)
	public String friendProductPage(Model model, HttpSession session) {
		ProductDTO dto = new ProductDTO();
		dto.setPageNo(1);
		dto.setEndpageNo(5);
		int userNo = (Integer)session.getAttribute("userNo");
		List<WishListDTO> wishList = service.getFriendWishList(userNo);
		List<ProductDTO> mainList = service.getFriendMainList(dto);	// 친구 상품목록 가져오기
		List<ProductDTO> photoList = service.getPhotoList(); // 친구 상품 사진목록 가져오기'
		List<ProdCommentDTO> commentList = service.getcommentList(); // 전체 댓글목록 가져오기
		model.addAttribute("mainList", mainList);
		model.addAttribute("photoList", photoList);
		model.addAttribute("commentList", commentList);
		model.addAttribute("wishList", wishList);
		
		return "main/friendProductPage";
	}
	
	@RequestMapping(value="/friendProductPage", method=RequestMethod.GET)
	public String friendProductPageGET(HttpServletRequest request, Model model, HttpSession session) {
		ProductDTO dto = new ProductDTO();
		dto.setCategory(request.getParameter("category")); // 카테고리 세팅
		dto.setPageNo(1);
		dto.setEndpageNo(5);
		int userNo = (Integer)session.getAttribute("userNo");
		// List<WishListDTO> wishList = service.getWishList(userNo);
		List<ProductDTO> mainList2 = service.getFriendMainList(dto);	// 전체 상품목록 가져오기
		List<ProductDTO> mainList = new ArrayList<ProductDTO>();
		for(ProductDTO pdto : mainList2) {
			pdto.setIsWish(service.isWish(pdto.getProductNo()));
			mainList.add(pdto);
		}
		// productNo = 상품번호 
		// SET 
		List<ProductDTO> photoList = service.getPhotoList(); // 전체 상품 사진목록 가져오기'
		List<ProdCommentDTO> commentList = service.getcommentList(); // 전체 댓글목록 가져오기
		model.addAttribute("mainList", mainList);
		model.addAttribute("photoList", photoList);
		model.addAttribute("commentList", commentList);
		// model.addAttribute("wishList", wishList);
		// System.out.println("wishList: " + wishList);
		return "main/mainpage";
	}

	@RequestMapping(value="/addMainList", method=RequestMethod.POST)
	@ResponseBody
	public List<ProductDTO> addmainList(@RequestParam("pageNo") int pageNo, @RequestParam("endpageNo") int endpageNo, Model model) {
		ProductDTO dto = new ProductDTO();
		dto.setPageNo((pageNo*5)+1);
		dto.setEndpageNo((endpageNo*5));
		List<ProductDTO> list = service.getMainList(dto);
		List<ProductDTO> mainList = new ArrayList<ProductDTO>();
		
		for(ProductDTO pdto : list) {
			pdto.setIsWish(service.isWish(pdto.getProductNo()));
			mainList.add(pdto);
		}
		model.addAttribute("AddList", mainList);
		System.out.println(mainList);
		return mainList;
	}
	
	@RequestMapping(value="/addphotoList", method=RequestMethod.POST)
	@ResponseBody
	public List<ProductDTO> addphotoList(Model model) {
		List<ProductDTO> photoList = service.getPhotoList();
		return photoList;
	}
	
	@RequestMapping(value = "/imageView", method = RequestMethod.GET)
	@ResponseBody
	public ImageView imageView(@RequestParam("fileName") String fileName, HttpSession session) throws FileNotFoundException, IOException {
		// ImageView 클래스를 만들고, setCotentType으로 이미지라는 것을 명시해준다.
		byte[] nn = null;
		String defaultPath = session.getServletContext().getRealPath("/");
		System.out.println("defaultPath : "+defaultPath);
		String filePath = defaultPath + File.separator + "uploadImg" + File.separator; // 파일경로 가져오기
		
		ImageView imageView = null;
		
		if(fileName == null){
			imageView = new ImageView("IMAGEVIEW");
		} else {
			imageView = new ImageView(filePath + "\\" + fileName);
		}
		
		return imageView;
	}
}
