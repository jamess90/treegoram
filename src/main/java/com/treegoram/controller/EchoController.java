package com.treegoram.controller;

import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.treegoram.dto.Hello;

@Controller
public class EchoController {
	@MessageMapping("/echo/{userId}")
	@SendTo("/subscribe/{userId}")
	public Hello sendEcho(@DestinationVariable(value="userId") String userId,Hello hello) throws Exception {
		System.out.println("receive message : " + hello.toString());
		System.out.println("userid : "+userId);
		return hello;
	}
}
