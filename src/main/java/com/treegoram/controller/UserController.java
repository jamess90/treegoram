package com.treegoram.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;

import com.treegoram.dto.UserDTO;
import com.treegoram.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(SessionStatus sessionStatus, HttpSession httpSession) {

		System.out.println("로그아웃1");
		sessionStatus.setComplete();
		httpSession.invalidate();
		System.out.println("로그아웃2");
		return "redirect:/";
	}

	@RequestMapping(value="/login", method=RequestMethod.GET)
   public String login(HttpSession httpSession) {
		
      return "/login";
   }

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpSession httpSession, UserDTO user) {

		System.out.println("패스워드: " + user.getPassword());
		user = userService.selectUser(user, user.getPassword());

		if (user != null) {
			System.out.println(user.getUserNo());
			httpSession.setAttribute("userNo", user.getUserNo());
			httpSession.setAttribute("userName", user.getUserName());
			httpSession.setAttribute("email", user.getEmail());

			System.out.println(httpSession.getAttribute("userNo"));
			System.out.println("로그인 성공");
			return "redirect:/mainpage";
		}

		System.out.println("로그인 실패");
		return "redirect:/";
	}

	@RequestMapping(value = "/join", method = RequestMethod.GET)
	public String join(HttpSession httpSession) {

		return "/join";
	}
	@RequestMapping(value = "/jointest", method = RequestMethod.POST)
	@ResponseBody
	public UserDTO joinTest(@ModelAttribute UserDTO user, String phone1, String phone2, String phone3,
			HttpSession httpSession, Model model){
		System.out.println(phone1 + "&" + phone2 + "&" + phone3);
		String phone = phone1 + phone2 + phone3;
		user.setPhone(phone);
		return user;
	}
	@RequestMapping(value = "/join", method = RequestMethod.POST)
	public String join(@ModelAttribute UserDTO user, String phone1, String phone2, String phone3,
			HttpSession httpSession, Model model)
			throws IllegalStateException, IOException {

		System.out.println(phone1 + "&" + phone2 + "&" + phone3);
		String phone = phone1 + phone2 + phone3;
		user.setPhone(phone);

		List files = user.getUpfiles();
		System.out.println(files);
		String saveDir = "";

		if (files != null) {
			System.out.println("files size:"+files.size());
			String defaultPath = httpSession.getServletContext().getRealPath("/");
			System.out.println("defaultPath: " + defaultPath);
			saveDir = defaultPath + File.separator + "uploadImg" + File.separator;

			File directory = new File(saveDir);
			if (!directory.exists()) {
				directory.mkdirs();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
			String today = formatter.format(new Date());
			for (Object f : files) {
				MultipartFile file = (MultipartFile) f;

				if (!file.isEmpty()) {
					String fileName = file.getOriginalFilename();

					String modifyName = today + "-" + UUID.randomUUID().toString().substring(20) + "." + fileName;
					System.out.println("fileName: " + fileName);
					System.out.println("modifyName: " + modifyName);

					File dest = new File(saveDir, modifyName);
					System.out.println(user.getUserNo());
					user.setUserPhoto(modifyName);
					System.out.println(userService.insertUser(user) + "개의 유저가 입력되었습니다.");
					System.out.println(user);

					file.transferTo(dest);
				}
				else{
					user.setUserPhoto("NULL");
					System.out.println(user);
					System.out.println(userService.insertUser(user) + "개의 유저가 입력되었습니다.");
				}
			}
		} else {
			user.setUserPhoto("NULL");
			userService.insertUser(user);
			System.out.println(user);
		}

		return "redirect:/";
	}
}