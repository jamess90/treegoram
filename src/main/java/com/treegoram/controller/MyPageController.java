package com.treegoram.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.swing.plaf.synth.SynthSeparatorUI;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.treegoram.dto.FindFriendDTO;
import com.treegoram.dto.FriendDTO;
import com.treegoram.dto.MyPageDTO;
import com.treegoram.service.MyPageService;

/**
 * Handles requests for the application home page.
 */

@Controller
public class MyPageController {
	@Autowired
	SqlSessionTemplate session;
	@Autowired
	MyPageService service;

	// 회원정보보기
	@RequestMapping(value = "/myInfo.do", method = RequestMethod.GET)
	public String mypage(Model model, HttpSession httpSession) {

		if (httpSession.getAttribute("userNo") == null)
			return "redirect:/";

		System.out.println(httpSession.getAttribute("userNo"));
		int userNo = (Integer) httpSession.getAttribute("userNo");
		System.out.println("userNo=" + userNo);
		MyPageDTO userInfo = service.selectUserInfo(userNo);
		System.out.println(userInfo);
		model.addAttribute("userInfo", userInfo);
		return "mypage/myInfo";
	}

	// 회원정보수정
	@RequestMapping(value = "/editInfo.do", method = RequestMethod.POST)
	public String editInfo(Model model, MyPageDTO dto, HttpSession httpSession) {

		if (httpSession.getAttribute("userNo") == null)
			return "redirect:/";
		
		dto.setUserNo((Integer) httpSession.getAttribute("userNo"));
		session.update("myPage.updateMyInfo", dto);
		return "redirect:/myInfo.do";
	}

	// 회원 탈퇴
	@RequestMapping(value = "secession.do", method = RequestMethod.POST)
	public String memberSecession(Model model, MyPageDTO dto, HttpSession httpSession) {

		if (httpSession.getAttribute("userNo") == null)
			return "redirect:/";
		
		int sessionUserNo = (Integer) httpSession.getAttribute("userNo");
		dto.setUserNo(sessionUserNo);
		
		boolean isdeletefriendInfo = service.deleteFriend(dto, httpSession);
		boolean isdeleteuserInfo = service.deleteUser(dto, httpSession);
		System.out.println("delete" + isdeleteuserInfo);
		return "redirect:/";
	}

	// 친구검색페이지
	@RequestMapping(value = "friendSearchPage.do", method = RequestMethod.GET)
	public String friendSearchPage(HttpSession httpSession, Model model, String searchWord, FindFriendDTO fdto) {

		if (httpSession.getAttribute("userNo") == null)
			return "redirect:/";
		
		if (searchWord == null)
			searchWord = "";
		fdto.setUserNo((Integer) httpSession.getAttribute("userNo"));
		fdto.setSearchWord(searchWord);
		List<MyPageDTO> friendsInfo = service.findFriend(fdto);
		model.addAttribute("friendsInfo", friendsInfo);
		model.addAttribute("fdto", fdto);

		return "mypage/searchList";
	}

	// 팔로우
	@RequestMapping(value = "follow.do", method = RequestMethod.GET)
	public String follow(HttpSession httpSession, Model model, String searchWord, FindFriendDTO fdto,
			FriendDTO frienddto, int userNo, int friendNo) {

		if (httpSession.getAttribute("userNo") == null)
			return "redirect:/";
		
		frienddto.setUserNo(userNo);
		frienddto.setFriendNo(friendNo);
		service.follow(frienddto);
		return friendSearchPage(httpSession, model, searchWord, fdto);
	}

	// 언팔로우
	@RequestMapping(value = "unfollow.do", method = RequestMethod.GET)
	public String unfollow(HttpSession httpSession, Model model, String searchWord, FindFriendDTO fdto,
			FriendDTO frienddto, int userNo, int friendNo) {

		if (httpSession.getAttribute("userNo") == null)
			return "redirect:/";
		

		frienddto.setUserNo(userNo);
		frienddto.setFriendNo(friendNo);
		service.unfollow(frienddto);
		return friendSearchPage(httpSession, model, searchWord, fdto);

	}
}
