package com.treegoram.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.treegoram.dto.Friend;
import com.treegoram.service.ChatingService;

@RestController
public class ChatingController {
	@Autowired
	ChatingService service;

	@RequestMapping(value = "/friends", method = RequestMethod.GET)
	public List<Friend> getFrieds(@RequestParam(value="user_no")int userNo){
		List<Friend> list=service.getFriends(userNo);
		System.out.println(list);
		return list;
	}
}