package com.treegoram.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.treegoram.dto.BidListDTO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;
import com.treegoram.service.BidListService;

@Controller
public class AllListController {

	@Autowired
	BidListService service;
	
	@RequestMapping(value="/tender.do", method=RequestMethod.GET) 
	public String joinTender(@RequestParam("productNo") int productNo, Model model) {
		// productNo에 해당하는 입찰참여목록 가져오기
		List<BidListDTO> bidList = service.getBidList(productNo);
		List<ProductDTO> dtoList = service.getInfor(productNo);
		ProductDTO dto = new ProductDTO();
		for(int i=0; i<dtoList.size(); i++) { 
			dto = dtoList.get(i);
		}
		
		// 상품명, 경매단위 가져오기
		model.addAttribute("productNo", productNo);
		model.addAttribute("bidList", bidList);
		model.addAttribute("pdto", dto);

		return "allList/tender";
	}
	
	@RequestMapping(value="/tender.do", method=RequestMethod.POST)
	@ResponseBody
	public List<BidListDTO> joinBid(@RequestParam("userName") String userName, 
			@RequestParam("userNo") String userNo, @RequestParam("productNo") String productNo,
			@RequestParam("bidPrice") String bidPrice) {
		// 입찰참여값 넣기
		BidListDTO dto = new BidListDTO();
		dto.setBidPrice(Integer.parseInt(bidPrice));
		dto.setProductNo(Integer.parseInt(productNo));
		dto.setUserName(userName);
		dto.setUserNo(Integer.parseInt(userNo));
	
		int res = service.insertTender(dto);
		List<BidListDTO> list = new ArrayList<BidListDTO>();
		if(res==1) {
			// 추가참여목록 리턴해주기
			list.add(dto);
			// current_price update 시켜주기
			int upRes = service.updatePrice(dto);
		}
		return list;
	} // tender.do() post end
	
	@RequestMapping(value="/promptTender.do", method=RequestMethod.POST)
	@ResponseBody
	public List<BidListDTO> promptTender(@RequestParam("userNo") String userNo, @RequestParam("productNo") String productNo,
							@RequestParam("bidPrice") String bidPrice) {

		BidListDTO dto = new BidListDTO();

		dto.setBidPrice(Integer.parseInt(bidPrice));
		dto.setProductNo(Integer.parseInt(productNo));
		dto.setUserNo(Integer.parseInt(userNo));
		int res = service.insertTender(dto);
		List<BidListDTO> list = new ArrayList<BidListDTO>();
		if(res==1) {
			list.add(dto);
			// 입찰종료, current_price, end_time=sysdate로
			int upRes = service.promptTender(dto);
		}
		
		return list;
	}
	
	@RequestMapping(value="/getComment.do", method=RequestMethod.POST)
	@ResponseBody
	public List<ProdCommentDTO> getCommentList(@RequestParam("productNo") int productNo) {
		List<ProdCommentDTO> commentList = service.getCommentList(productNo);
		return commentList;
	}
	
	@RequestMapping(value="/rmComment.do", method=RequestMethod.POST)
	@ResponseBody
	public int removeComment(@RequestParam("commentNo") String commentNo) {
		int result = service.removeComment(Integer.parseInt(commentNo));
		return result;
	}
	@RequestMapping(value="/addComment.do", method=RequestMethod.POST)
	@ResponseBody
	public ProdCommentDTO addComment(@RequestParam("userName") String userName, @RequestParam("text") String text,
							@RequestParam("userNo") String userNo, @RequestParam("productNo") String productNo) {
		ProdCommentDTO dto = new ProdCommentDTO();
		dto.setProductNo(Integer.parseInt(productNo));
		dto.setUserNo(Integer.parseInt(userNo));
		dto.setUserName(userName);
		String realText = text.replaceAll("\r\n", "<br/>");
		dto.setText(realText);
		int res = service.insertComment(dto);
		
		
		int commentNo = service.selectCno();
		ProdCommentDTO pdto = new ProdCommentDTO();
		String regDate = service.getRegDate(dto.getProductNo());
		pdto.setRegDate(regDate);
		pdto.setCommentNo(commentNo-1);
		return pdto;
	}
	@RequestMapping(value="/rmWishList.do", method=RequestMethod.POST)
	@ResponseBody
	public int rmWishList(@RequestParam("userNo") String userNo, @RequestParam("productNo") String productNo) {
		WishListDTO dto = new WishListDTO();
		dto.setProductNo(Integer.parseInt(productNo));
		dto.setUserNo(Integer.parseInt(userNo));
		
		int result = service.rmWishList(dto);
		return result;
	}
	
	@RequestMapping(value="/addWishList.do", method=RequestMethod.POST)
	@ResponseBody
	public int addWishList(@RequestParam("userNo") String userNo, @RequestParam("productNo") String productNo) {
		WishListDTO dto = new WishListDTO();
		dto.setProductNo(Integer.parseInt(productNo));
		dto.setUserNo(Integer.parseInt(userNo));
		
		
		int result = service.addWishList(dto);
		return result;
	}
	
}
