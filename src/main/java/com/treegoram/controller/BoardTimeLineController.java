package com.treegoram.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.treegoram.dto.TimeLineCommentDTO;
import com.treegoram.dto.TimeLineDTO;
import com.treegoram.dto.TimeLinePhoto;
import com.treegoram.service.TimeLineService;

@Controller
public class BoardTimeLineController {
	
	@Autowired
	private TimeLineService tservice;
	
	@RequestMapping(value = "/timelinepage", method = RequestMethod.GET)
	public String timelinepage(@RequestParam(defaultValue="1") int page, ModelMap map){
		System.out.println("page : "+page);
		//사진과 조인한 타임라인 List를 가지고 오는것.
		List<TimeLineDTO> list = tservice.selectPaging(page);
		System.out.println(list);
		for(int i=0;i<list.size();i++){
			list.get(i).setUserName(tservice.selectuserTitlebyNo(list.get(i).getUserNo()));
			list.get(i).setUserPhoto(tservice.userPhotoByuserNo(list.get(i).getUserNo()));
		}
		for(int j=0;j<list.size();j++){
			list.get(j).setPhotoList(tservice.selectPhototableBytimelineNo(list.get(j).getTimelineNo()));
		}

		System.out.println("list : "+list);
		System.out.println("list의 사이즈 : "+list.size());

		map.addAttribute("timelineList", list);
		map.addAttribute("page",1);
		return "BoardTimeLine/BoardTimeLine";
	}
	
	
	
	@RequestMapping(value = "/thebogi", method = RequestMethod.POST)
	@ResponseBody
	public List<TimeLineDTO> timelinepageThebogi(int page, ModelMap map){
		System.out.println("page : "+page);
		
		//사진과 조인한 타임라인 List를 가지고 오는것.
		List<TimeLineDTO> list = tservice.selectPaging(page);
		System.out.println(list);
		
		for(int i=0;i<list.size();i++){
			list.get(i).setUserName(tservice.selectuserTitlebyNo(list.get(i).getUserNo()));
			list.get(i).setUserPhoto(tservice.userPhotoByuserNo(list.get(i).getUserNo()));
		}
		for(int j=0;j<list.size();j++){
			list.get(j).setPhotoList(tservice.selectPhototableBytimelineNo(list.get(j).getTimelineNo()));
		}

		System.out.println("list : "+list);
		System.out.println("list의 사이즈 : "+list.size());

		map.addAttribute("timelineList", list);
		map.addAttribute("page",page);
		
		return list;
	}
	
	
	
	
	@RequestMapping(value ="/timelineRegister", method= RequestMethod.POST)
	@Transactional
	public String timeLineRegister(@ModelAttribute TimeLineDTO td, BindingResult errors, ModelMap map, HttpSession session) throws IllegalStateException, IOException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		td.setRegDate(sdf.format(new Date()));
		System.out.println(td);
		System.out.println(tservice.insertTimeLineRegister(td)+"개의 타임라인이 등록되었습니다.");
		td.setTimelineNo(tservice.selectTimeLineNo());
		List files = td.getUpfiles();
		
		if(files!=null){//upfile이름의 요청파라미터가 있는경우.
			//String saveDir = request.getSession().getServletContext().getRealPath("classpath:");
			String saveDir = session.getServletContext().getRealPath("\\")+"\\timelineIMG";
			System.out.println(saveDir);
			for(Object f : files){
				MultipartFile file = (MultipartFile)f; //업로드된 파일정보 하나.
				if(!file.isEmpty()){//전송된 파일이 있다면
					String fileName = td.getTimelineNo()+"_"+file.getOriginalFilename();
					System.out.println(fileName);
					File dest = new File(saveDir, fileName);
					
					//userNo는 session값으로 날라오는것을 가지고
					System.out.println(tservice.insertTimeLinephoto(td.getTimelineNo(), fileName)+"개의 사진이 추가되었습니다.");
					file.transferTo(dest);
				}
			}
		}
		
		List<TimeLineDTO> list = tservice.selectTimeLineAll();
		
		for(int i=0;i<list.size();i++){
			list.get(i).setUserName(tservice.selectuserTitlebyNo(list.get(i).getUserNo()));
		}
		System.out.println("list : "+list);
		
		map.addAttribute("timelineList", list);
		
		return "redirect:/timelinepage";
	}
	
	
	@RequestMapping(value ="/timelineDelete", method= RequestMethod.GET)
	public String timelineDelete(@RequestParam(defaultValue="1") int page, int timelineNo, ModelMap map, HttpSession session){
		System.out.println(tservice.timelineDelete(timelineNo)+"개의 타임라인 삭제");
		System.out.println("page : "+page);
		
		
		//사진과 조인한 타임라인 List를 가지고 오는것.
		List<TimeLineDTO> list = tservice.selectPaging(page);
		System.out.println(list);
		
		for(int i=0;i<list.size();i++){
			list.get(i).setUserName(tservice.selectuserTitlebyNo(list.get(i).getUserNo()));
			list.get(i).setUserPhoto(tservice.userPhotoByuserNo(list.get(i).getUserNo()));
		}
		for(int j=0;j<list.size();j++){
			list.get(j).setPhotoList(tservice.selectPhototableBytimelineNo(list.get(j).getTimelineNo()));
		}

		System.out.println("list : "+list);
		System.out.println("list의 사이즈 : "+list.size());

		map.addAttribute("timelineList", list);
		map.addAttribute("page",page);
		map.addAttribute("deleteTimeline","선택하신 타임라인이 삭제가 되었습니다.");
		return "BoardTimeLine/BoardTimeLine";
	}
	
	@RequestMapping(value ="/insertTimelineComment.do", method= RequestMethod.POST)
	@ResponseBody
	public List<TimeLineCommentDTO> insertTimeLineComment(String datgle,int userNo, int timelineNo, ModelMap map){
		System.out.println(datgle+"&"+userNo+"&"+timelineNo);	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		
		//댓글 입력
		System.out.println(tservice.insertTimelineComment(new TimeLineCommentDTO(0, timelineNo, datgle, userNo, sdf.format(new Date())))+"개의 행 댓글을 입력되었습니다.");
		
		//댓글 조회 ajax 처리 
		List<TimeLineCommentDTO> list = tservice.selectCommentByTimelineNo(timelineNo);
		System.out.println("댓글조회 :"+list);
		
		String userName;
		//userNo로 회원이름을 찾아서 setter에다가 넣기
		for(int i=0;i<list.size();i++){
			userName = tservice.selectUserNameByUserNo(list.get(i).getUserNo());
			list.get(i).setUserName(userName);
		}
		return list;
	}
	
	
	@RequestMapping(value ="/commentShow.do", method= RequestMethod.POST)
	@ResponseBody
	public List<TimeLineCommentDTO> commentShow(int timelineNo){
		System.out.println("타임라인번호 : "+timelineNo);
		
		List<TimeLineCommentDTO> list = tservice.selectCommentByTimelineNo(timelineNo);
		System.out.println("댓글조회 :"+list);
		
		String userName;
		//userNo로 회원이름을 찾아서 setter에다가 넣기
		for(int i=0;i<list.size();i++){
			userName = tservice.selectUserNameByUserNo(list.get(i).getUserNo());
			list.get(i).setUserName(userName);
		}
		return list;
	}
	
	
	
	@RequestMapping(value ="/updateTimelinePage", method= RequestMethod.GET)
	public String updateTimelinePage(int timelineNo, ModelMap map){
		
		TimeLineDTO td = tservice.selectTimeLineByNo(timelineNo).get(0);
		map.addAttribute("timelineList",td);
		
		return "BoardTimeLine/BoardTimelineUpdate";
	}
	
	@RequestMapping(value ="/updateTimeline", method= RequestMethod.POST)
	public String updateTimeline(@ModelAttribute TimeLineDTO tld,BindingResult errors, ModelMap map,HttpSession session) throws IllegalStateException, IOException{
		int timelineNo = tld.getTimelineNo();
		String timelineTitle = tld.getTimelineTitle();
		String timelineContent = tld.getTimelineContent();
		System.out.println(tld);
		
		//timeline 테이블의 내용을 update하기
		System.out.println(tservice.updateTimelineByNo(timelineNo, timelineTitle, timelineContent)+"개의 행을 타임라인을 수정");
				
		
		List files = tld.getUpfiles();
		if(files != null){
			System.out.println(tservice.deleteTimelineByNo(timelineNo)+"개 삭제");	// photo테이블에서 삭제		
		}
		
		if(files!=null){//upfile이름의 요청파라미터가 있는경우.
			//String saveDir = request.getSession().getServletContext().getRealPath("classpath:/");
			String saveDir = session.getServletContext().getRealPath("/")+"\\timelineIMG";
			System.out.println(saveDir);
			for(Object f : files){
				MultipartFile file = (MultipartFile)f; //업로드된 파일정보 하나.
				if(!file.isEmpty()){//전송된 파일이 있다면
					String fileName = timelineNo+"_"+file.getOriginalFilename();
					System.out.println(fileName);
					File dest = new File(saveDir, fileName);
					
					//userNo는 session값으로 날라오는것을 가지고
					System.out.println(tservice.insertTimeLinephoto(timelineNo, fileName)+"개의 사진이 추가되었습니다.");
					file.transferTo(dest);
				}
			}
		}
		
		return "redirect:/timelinepage";
	}
	
	
	@RequestMapping(value ="/deleteCommentByTcNum", method= RequestMethod.POST)
	@ResponseBody
	public List<TimeLineCommentDTO> deleteCommentByTcNum(int tcnum, int timelineNo, ModelMap map){
		System.out.println(tcnum+"&"+timelineNo);
		System.out.println(tservice.deleteCommentByTcNum(tcnum)+"개의 댓글을 삭제하였습니다.");
		
		List<TimeLineCommentDTO> list = tservice.selectCommentByTimelineNo(timelineNo);
		System.out.println("댓글조회 :"+list);
		
		String userName;
		//userNo로 회원이름을 찾아서 setter에다가 넣기
		for(int i=0;i<list.size();i++){
			userName = tservice.selectUserNameByUserNo(list.get(i).getUserNo());
			list.get(i).setUserName(userName);
		}
		return list;
	}
}
