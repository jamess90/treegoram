package com.treegoram.dto;

public class WishListDTO {

	private int userNo;
	private int productNo;
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public int getProductNo() {
		return productNo;
	}
	public void setProductNo(int productNo) {
		this.productNo = productNo;
	}
	@Override
	public String toString() {
		return "WishListDTO [userNo=" + userNo + ", productNo=" + productNo + "]";
	}
	
	
}
