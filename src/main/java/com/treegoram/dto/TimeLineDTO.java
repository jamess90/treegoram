package com.treegoram.dto;

import java.io.Serializable;
import java.util.List;

public class TimeLineDTO implements Serializable{

	private int timelineNo;
	private String timelineTitle;
	private String timelineContent;
	private int userNo;
	private String regDate;
	private String userName;
	private String userPhoto;
	private List upfiles;
	
	public String getUserPhoto() {
		return userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	private List<TimeLinePhoto> photoList;
	private List<TimeLineCommentDTO> commentList;
	
	public TimeLineDTO(){}
	
	public int getTimelineNo() {
		return timelineNo;
	}

	public void setTimelineNo(int timelineNo) {
		this.timelineNo = timelineNo;
	}

	public String getTimelineTitle() {
		return timelineTitle;
	}

	public void setTimelineTitle(String timelineTitle) {
		this.timelineTitle = timelineTitle;
	}

	public String getTimelineContent() {
		return timelineContent;
	}

	public void setTimelineContent(String timelineContent) {
		this.timelineContent = timelineContent;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	public List getUpfiles() {
		return upfiles;
	}

	public void setUpfiles(List upfiles) {
		this.upfiles = upfiles;
	}
	
	

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<TimeLinePhoto> getPhotoList() {
		return photoList;
	}

	public void setPhotoList(List<TimeLinePhoto> photoList) {
		this.photoList = photoList;
	}
	

	public List<TimeLineCommentDTO> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<TimeLineCommentDTO> commentList) {
		this.commentList = commentList;
	}

	@Override
	public String toString() {
		return "TimeLineDTO [timelineNo=" + timelineNo + ", timelineTitle=" + timelineTitle + ", timelineContent="
				+ timelineContent + ", userNo=" + userNo + ", regDate=" + regDate + ", userName=" + userName
				+ ", userPhoto=" + userPhoto + ", upfiles=" + upfiles + ", photoList=" + photoList + ", commentList="
				+ commentList + "]";
	}



	
}
