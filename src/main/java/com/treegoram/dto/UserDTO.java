package com.treegoram.dto;

import java.sql.Date;
import java.util.List;

public class UserDTO {
	private int userNo;
	private String email;
	private String password;
	private String userName;
	private String phone;
	private int userPostNumber;
	private String userAddress1;
	private String userAddress2;
	private Date birthday;
	private String userPhoto;
	private List upfiles;
	private int penalty;
	private String regDate;
	
	public UserDTO() {
		
	}
	
	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getUserPostNumber() {
		return userPostNumber;
	}

	public void setUserPostNumber(int userPostNumber) {
		this.userPostNumber = userPostNumber;
	}

	public String getUserAddress1() {
		return userAddress1;
	}

	public void setUserAddress1(String userAddress1) {
		this.userAddress1 = userAddress1;
	}

	public String getUserAddress2() {
		return userAddress2;
	}

	public void setUserAddress2(String userAddress2) {
		this.userAddress2 = userAddress2;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getUserPhoto() {
		return userPhoto;
	}

	public void setUserPhoto(String userPhoto) {
		this.userPhoto = userPhoto;
	}

	public List getUpfiles() {
		return upfiles;
	}

	public void setUpfiles(List upfiles) {
		this.upfiles = upfiles;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}

	public String getRegDate() {
		return regDate;
	}

	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}

	@Override
	public String toString() {
		return "UserDTO [userNo=" + userNo + ", email=" + email + ", password=" + password + ", userName=" + userName
				+ ", phone=" + phone + ", userPostNumber=" + userPostNumber + ", userAddress1=" + userAddress1
				+ ", userAddress2=" + userAddress2 + ", birthday=" + birthday + ", userPhoto=" + userPhoto
				+ ", upfiles=" + upfiles + ", penalty=" + penalty + ", regDate=" + regDate + "]";
	}
}