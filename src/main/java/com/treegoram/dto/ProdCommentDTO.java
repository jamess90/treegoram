package com.treegoram.dto;

public class ProdCommentDTO {
	private int commentNo;
	private int productNo;
	private int userNo;
	private String text;
	private String regDate;
	private String userName;
	
	public int getCommentNo() {
		return commentNo;
	}
	public void setCommentNo(int commentNo) {
		this.commentNo = commentNo;
	}
	public int getProductNo() {
		return productNo;
	}
	public void setProductNo(int productNo) {
		this.productNo = productNo;
	}
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "ProdCommentDTO [commentNo=" + commentNo + ", productNo=" + productNo + ", userNo=" + userNo + ", text="
				+ text + ", regDate=" + regDate + ", userName=" + userName + "]";
	}
	
	
	
	
	
}
