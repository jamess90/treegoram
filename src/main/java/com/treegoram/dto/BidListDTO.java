package com.treegoram.dto;

public class BidListDTO {
	private int productNo;				// 상품번호
	private int userNo;					// 회원번호
	private int bidPrice;				// 입찰참여가
	private String bidSuccess;			// 낙찰여부
	private String userName;			// 회원이름

	
	
	public int getProductNo() {
		return productNo;
	}

	public void setProductNo(int productNo) {
		this.productNo = productNo;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public int getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(int bidPrice) {
		this.bidPrice = bidPrice;
	}

	public String getBidSuccess() {
		return bidSuccess;
	}

	public void setBidSuccess(String bidSuccess) {
		this.bidSuccess = bidSuccess;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "BidListDTO [productNo=" + productNo + ", userNo=" + userNo + ", bidPrice=" + bidPrice + ", bidSuccess="
				+ bidSuccess + ", userName=" + userName + "]";
	}
	
	



	

	
	
	
	
}
