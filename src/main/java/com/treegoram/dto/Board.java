package com.treegoram.dto;

import java.io.Serializable;
import java.util.List;

public class Board implements Serializable{

	private String title;
	private String content;
	//input type=file ?���??��?��미터 ???��. - ?��?��:MultipartFile ???�� �??��, ?��?���?:List
	private List upfile; //List <- MutlipartFile 객체?��
	private List<String> upflieName; //B.L 처리?��
	
	public Board(){}

	public Board(String title, String content, List upfile) {
		this.title = title;
		this.content = content;
		this.upfile = upfile;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List getUpfile() {
		return upfile;
	}

	public void setUpfile(List upfile) {
		this.upfile = upfile;
	}

	@Override
	public String toString() {
		return "Board [title=" + title + ", content=" + content + ", upfile=" + upfile + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((upfile == null) ? 0 : upfile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Board other = (Board) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (upfile == null) {
			if (other.upfile != null)
				return false;
		} else if (!upfile.equals(other.upfile))
			return false;
		return true;
	}
	
	
}








