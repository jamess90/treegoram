package com.treegoram.dto;

import java.io.Serializable;

public class TimeLinePhoto implements Serializable{
	private int timelineNo;
	private String timelinePhotoFile;
	
	public TimeLinePhoto(){
		
	}

	public TimeLinePhoto(int timelineNo, String timelinePhotoFile) {
		super();
		this.timelineNo = timelineNo;
		this.timelinePhotoFile = timelinePhotoFile;
	}

	public int getTimelineNo() {
		return timelineNo;
	}

	public void setTimelineNo(int timelineNo) {
		this.timelineNo = timelineNo;
	}

	public String getTimelinePhotoFile() {
		return timelinePhotoFile;
	}

	public void setTimelinePhotoFile(String timelinePhotoFile) {
		this.timelinePhotoFile = timelinePhotoFile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + timelineNo;
		result = prime * result + ((timelinePhotoFile == null) ? 0 : timelinePhotoFile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeLinePhoto other = (TimeLinePhoto) obj;
		if (timelineNo != other.timelineNo)
			return false;
		if (timelinePhotoFile == null) {
			if (other.timelinePhotoFile != null)
				return false;
		} else if (!timelinePhotoFile.equals(other.timelinePhotoFile))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TimeLinePhoto [timelineNo=" + timelineNo + ", timelinePhotoFile=" + timelinePhotoFile + "]";
	}
	
	
	
}
