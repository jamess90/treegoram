package com.treegoram.dto;


import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class ProductDTO {
   private int productNo;
   private String productName;
   private String category;
   private int startPrice;
   private int raiseUnit;
   private int currentPrice;
   private String text;
   private int likeNumber;
   private int promptPrice;
   private int userNo;
   private String userName;
   private String startTime;
   private Timestamp endDate;
   private List upfiles;
   private String productPhoto;
   private List<ProdCommentDTO> comments; 
   private int pageNo;
   private int endpageNo;
   private int isWish;
   private String userPhoto;
   
   
   public ProductDTO(){}


public int getProductNo() {
	return productNo;
}


public void setProductNo(int productNo) {
	this.productNo = productNo;
}


public String getProductName() {
	return productName;
}


public void setProductName(String productName) {
	this.productName = productName;
}


public String getCategory() {
	return category;
}


public void setCategory(String category) {
	this.category = category;
}


public int getStartPrice() {
	return startPrice;
}


public void setStartPrice(int startPrice) {
	this.startPrice = startPrice;
}


public int getRaiseUnit() {
	return raiseUnit;
}


public void setRaiseUnit(int raiseUnit) {
	this.raiseUnit = raiseUnit;
}


public int getCurrentPrice() {
	return currentPrice;
}


public void setCurrentPrice(int currentPrice) {
	this.currentPrice = currentPrice;
}


public String getText() {
	return text;
}


public void setText(String text) {
	this.text = text;
}


public int getLikeNumber() {
	return likeNumber;
}


public void setLikeNumber(int likeNumber) {
	this.likeNumber = likeNumber;
}


public int getPromptPrice() {
	return promptPrice;
}


public void setPromptPrice(int promptPrice) {
	this.promptPrice = promptPrice;
}


public int getUserNo() {
	return userNo;
}


public void setUserNo(int userNo) {
	this.userNo = userNo;
}


public String getStartTime() {
	return startTime;
}


public void setStartTime(String startTime) {
	this.startTime = startTime;
}


public Timestamp getEndDate() {
	return endDate;
}


public void setEndDate(Timestamp endDate) {
	this.endDate = endDate;
}


public List<ProdCommentDTO> getComments() {
	return comments;
}


public void setComments(List<ProdCommentDTO> comments) {
	this.comments = comments;
}


public String getProductPhoto() {
	return productPhoto;
}


public void setProductPhoto(String productPhoto) {
	this.productPhoto = productPhoto;
}



public List getUpfiles() {
	return upfiles;
}


public void setUpfiles(List upfiles) {
	this.upfiles = upfiles;
}



public int getPageNo() {
	return pageNo;
}


public void setPageNo(int pageNo) {
	this.pageNo = pageNo;
}


public int getEndpageNo() {
	return endpageNo;
}


public void setEndpageNo(int endpageNo) {
	this.endpageNo = endpageNo;
}


public String getUserName() {
	return userName;
}


public void setUserName(String userName) {
	this.userName = userName;
}

public int getIsWish() {
	return isWish;
}


public void setIsWish(int isWish) {
	this.isWish = isWish;
}





public String getUserPhoto() {
	return userPhoto;
}


public void setUserPhoto(String userPhoto) {
	this.userPhoto = userPhoto;
}


@Override
public String toString() {
	return "ProductDTO [productNo=" + productNo + ", productName=" + productName + ", category=" + category
			+ ", startPrice=" + startPrice + ", raiseUnit=" + raiseUnit + ", currentPrice=" + currentPrice + ", text="
			+ text + ", likeNumber=" + likeNumber + ", promptPrice=" + promptPrice + ", userNo=" + userNo
			+ ", userName=" + userName + ", startTime=" + startTime + ", endDate=" + endDate + ", upfiles=" + upfiles
			+ ", productPhoto=" + productPhoto + ", pageNo=" + pageNo + ", endpageNo=" + endpageNo + ", isWish="
			+ isWish + ", userPhoto=" + userPhoto + "]";
}












   
}
