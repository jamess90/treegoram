package com.treegoram.dto;

import java.io.Serializable;

public class TimeLineCommentDTO implements Serializable{

	private int tcommentNo;
	private int timelineNo;
	private String timelineContent;
	private int userNo;
	private String timelineTime;
	private String userName;
	
	
	public TimeLineCommentDTO(){}

	public TimeLineCommentDTO(int tcommentNo, int timelineNo, String timelineContent, int userNo, String timelineTime) {
		super();
		this.tcommentNo = tcommentNo;
		this.timelineNo = timelineNo;
		this.timelineContent = timelineContent;
		this.userNo = userNo;
		this.timelineTime = timelineTime;
	}
	
	

	public TimeLineCommentDTO(int tcommentNo, int timelineNo, String timelineContent, int userNo, String timelineTime,
			String userName) {
		super();
		this.tcommentNo = tcommentNo;
		this.timelineNo = timelineNo;
		this.timelineContent = timelineContent;
		this.userNo = userNo;
		this.timelineTime = timelineTime;
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getTcommentNo() {
		return tcommentNo;
	}

	public void setTcommentNo(int tcommentNo) {
		this.tcommentNo = tcommentNo;
	}

	public int getTimelineNo() {
		return timelineNo;
	}

	public void setTimelineNo(int timelineNo) {
		this.timelineNo = timelineNo;
	}

	public String getTimelineContent() {
		return timelineContent;
	}

	public void setTimelineContent(String timelineContent) {
		this.timelineContent = timelineContent;
	}

	public int getUserNo() {
		return userNo;
	}

	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}

	public String getTimelineTime() {
		return timelineTime;
	}

	public void setTimelineTime(String timelineTime) {
		this.timelineTime = timelineTime;
	}

	@Override
	public String toString() {
		return "TimeLineCommentDTO [tcommentNo=" + tcommentNo + ", timelineNo=" + timelineNo + ", timelineContent="
				+ timelineContent + ", userNo=" + userNo + ", timelineTime=" + timelineTime + ", userName=" + userName
				+ "]";
	}
}
