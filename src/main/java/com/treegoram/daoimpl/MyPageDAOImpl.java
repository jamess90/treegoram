package com.treegoram.daoimpl;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.treegoram.dao.MyPageDAO;
import com.treegoram.dto.FindFriendDTO;
import com.treegoram.dto.FriendDTO;
import com.treegoram.dto.MyPageDTO;

@Repository
public class MyPageDAOImpl implements MyPageDAO {

	@Autowired
	SqlSessionTemplate session;

	@Override
	public MyPageDTO selectUserInfo(int userNo) {
		
		return session.selectOne("myPage.selectMyInfo", userNo);
	}
	
	@Override
	public List<MyPageDTO> selectMyFriendsList(int userNo) {
		
		return session.selectList("myPage.selectMyFriends", userNo);
	}

	@Override
	public int updateUserInfo(MyPageDTO dto) {

		int result = session.update("myPage.updateMyInfo", dto);
		System.out.println("수정된 내용");
		System.out.println(dto.toString());
		return result;
	}
	
	@Override
	public int deleteUser(int userNo) {
		
		int result = session.delete("myPage.deleteUser", userNo);
		return result;
	}
	
	@Override
	public int deleteFriend(int userNo) {
		
		int result = session.delete("myPage.deleteFriend", userNo);
		session.delete("myPage.deleteProduct", userNo);
		return result;
	}
	
	@Override
	public String selectUserPassword(int userNo) {

		return session.selectOne("myPage.selectPassword", userNo);
	}
	
	@Override
	public List<MyPageDTO> findFriend(FindFriendDTO fdto) {
		
		return session.selectList("friend.findfriend", fdto);
	}
	
	public int follow(FriendDTO frienddto){
		
		return session.insert("friend.follow",frienddto);
	}

	public int unfollow(FriendDTO frienddto){
		
		return session.delete("friend.unfollow",frienddto);
	}
}

