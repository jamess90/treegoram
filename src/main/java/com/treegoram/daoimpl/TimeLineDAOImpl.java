package com.treegoram.daoimpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.treegoram.dao.TimeLineDAO;
import com.treegoram.dto.TimeLineCommentDTO;
import com.treegoram.dto.TimeLineDTO;
import com.treegoram.dto.TimeLinePhoto;

@Repository
public class TimeLineDAOImpl implements TimeLineDAO{

	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public int selectTimeLineSEQ() {
		// TODO Auto-generated method stub
		return session.selectOne("selectTimeLineNoSeq");
	}

	@Override
	public int insertTimeLineRegister(TimeLineDTO td) {
		// TODO Auto-generated method stub
		HashMap<String,Object> param = new HashMap();
		param.put("timelineNo",td.getTimelineNo());
		param.put("timelineTitle",td.getTimelineTitle());
		param.put("timelineContent",td.getTimelineContent());
		param.put("userNo",td.getUserNo());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date to;
		try {
			to = sdf.parse(td.getRegDate());
			param.put("regDate",to);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return session.insert("timeLineRegister", param);
	}

	@Override
	public int insertTimeLinephoto(int timelineNo, String photoName) {
		// TODO Auto-generated method stub
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("timelineNo", timelineNo);
		param.put("photoName", photoName);
		return session.insert("timeLinePhoto", param);
	}

	@Override
	public List<TimeLineDTO> selectTimeLineAll() {
		// TODO Auto-generated method stub
		return session.selectList("selectTimeLineNo");
	}

	@Override
	public String selectuserTitlebyNo(int userNo) {
		// TODO Auto-generated method stub
		return session.selectOne("selectuserTitlebyNo", userNo);
	}

	@Override
	public int timelineDelete(int timelineNo) {
		// TODO Auto-generated method stub
		return session.delete("timelineDeleteByTimelineNo",timelineNo);
	}

	@Override
	public int insertTimelineComment(TimeLineCommentDTO tcd) {
		// TODO Auto-generated method stub
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("tcommentNo",tcd.getTcommentNo());
		param.put("timelineNo",tcd.getTimelineNo() );
		param.put("timelineContent", tcd.getTimelineContent() );
		param.put("userNo", tcd.getUserNo() );
		param.put("timelineTime", tcd.getTimelineTime());
		
		return session.insert("timelinecommentInsert", param);
	}

	@Override
	public List<TimeLineCommentDTO> selectCommentByTimelineNo(int timelineNo) {
		// TODO Auto-generated method stub
		return session.selectList("timelineComment", timelineNo);
	}

	@Override
	public String selectUserNameByUserNo(int userNo) {
		// TODO Auto-generated method stub
		return session.selectOne("selectUserNameByUserNo",userNo);
	}
	
	
	@Override
	public List<TimeLineDTO> selectTimeLineByNo(int timelineNo) {
		// TODO Auto-generated method stub
		return session.selectList("selectTimeLineByNo",timelineNo);
	}

	@Override
	public int updateTimelineByNo(int timelineNo, String timelineTitle, String timelineContent) {
		// TODO Auto-generated method stub
		HashMap<String, Object> param = new HashMap();
		param.put("timelineTitle", timelineTitle);
		param.put("timelineContent", timelineContent);
		param.put("timelineNo", timelineNo);
		
		return session.update("updateTimelineByNo",param);
	}

	@Override
	public int updatePhotoNameByNo(int timelineNo, String timelinePhotoFile) {
		// TODO Auto-generated method stub
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("timelineNo", timelineNo);
		param.put("timelinePhotoFile", timelinePhotoFile);
		return session.update("updatePhotoNameByNo", param);
	}

	@Override
	public int deleteTimelineByNo(int timelineNo) {
		// TODO Auto-generated method stub
		return session.delete("deleteTimelineByNo", timelineNo);
	}

	@Override
	public int selectTimeLineNo() {
		// TODO Auto-generated method stub
		return session.selectOne("timelineMapper.selectTimelineNoOne");
	}

	@Override
	public int deleteCommentByTcNum(int tcnum) {
		// TODO Auto-generated method stub
		return session.delete("deleteCommentByTcNum", tcnum);
	}

	@Override
	public List<TimeLineDTO> selectPaging(int page) {
		// TODO Auto-generated method stub
		return session.selectList("selectPaging",page);
	}

	@Override
	public List<TimeLinePhoto> selectPhototableBytimelineNo(int timelineNo) {
		// TODO Auto-generated method stub
		return session.selectList("selectPhototableBytimelineNo", timelineNo);
	}

	@Override
	public String userPhotoByuserNo(int userNo) {
		// TODO Auto-generated method stub
		return session.selectOne("userPhotoByuserNo", userNo);
	}
	
	
}
