package com.treegoram.daoimpl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.treegoram.dao.BidListDAO;
import com.treegoram.dto.BidListDTO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;

@Repository
public class BidListDAOImpl implements BidListDAO {

	@Autowired
	SqlSessionTemplate session;
	
	@Override
	public List<BidListDTO> getBidList(int productNo) {			// 입찰목록 가져오기
		List<BidListDTO> bidList = session.selectList("bidListMapper.getBidList", productNo);
		return bidList;
	}

	@Override
	public int insertTender(BidListDTO dto) {
		int res = session.insert("bidListMapper.insertTender", dto);
		return res;
	}

	@Override
	public int updateTender(BidListDTO dto) {
		int res = session.update("bidListMapper.updateTender", dto);
		return res;
	}

	@Override
	public int insertComment(ProdCommentDTO dto) {
		return session.insert("bidListMapper.insertComment", dto);
	}

	@Override
	public String getRegDate(int productNo) {
		return session.selectOne("bidListMapper.getRegDate", productNo);
	}

	@Override
	public List<ProdCommentDTO> getCommentList(int productNo) {
		return session.selectList("bidListMapper.getCommentsList", productNo);
	}

	@Override
	public int rmWishList(WishListDTO dto) {
		return session.delete("bidListMapper.rmWishList", dto);
	}

	@Override
	public int addWishList(WishListDTO dto) {
		return session.insert("bidListMapper.addWishList", dto);
	}

	@Override
	public int removeComment(int commentNo) {
		return session.delete("bidListMapper.removeComment", commentNo);
	}

	@Override
	public int selectCno() {
		return session.selectOne("bidListMapper.selectCno");
	}

	@Override
	public List<ProductDTO> getInfor(int productNo) {
		return session.selectList("bidListMapper.getInfor", productNo);
	}

	@Override
	public int promptTender(BidListDTO dto) {
		return session.update("bidListMapper.promptTender", dto);
	}

	
	
	
	
	

	
}
