package com.treegoram.daoimpl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.treegoram.dao.ChatingDAO;
import com.treegoram.dto.Friend;

@Repository
public class ChatingDAOImpl implements ChatingDAO {
	@Autowired
	SqlSessionTemplate sqlSession;
	
	@Override
	public List<Friend> getFriends(int userNo) {
		return sqlSession.selectList("chatingMapper.selectFriends", userNo);
	}

}
