package com.treegoram.daoimpl;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.treegoram.dao.ProductDAO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;

@Repository
public class ProductDAOImpl implements ProductDAO{

	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public int InsertProduct(ProductDTO pdo) {
		// TODO Auto-generated method stu
		return session.insert("productMapper.insertProduct", pdo);
	}

	@Override
	public int insertTest(int no) {
		// TODO Auto-generated method stub
		return session.insert("productMapper.insertTest",no);
	}

	@Override
	public int selectProductNoSeq() {
		// TODO Auto-generated method stub
		return session.selectOne("productMapper.selectProductNoSeq");
	}

	@Override
	public int insertProductPhoto(int productNo, String photoName) {
		// TODO Auto-generated method stub
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("productNo",productNo);
		param.put("photoName", photoName);
		return session.insert("insertProductPhoto", param);
	}
	
	@Override
	public List<ProductDTO> getMainList(ProductDTO dto) {		// 상품목록 가져오기
		return session.selectList("productMapper.getMainList", dto);
	} // getMainList() end

	@Override
	public List<ProductDTO> getFriendMainList(ProductDTO dto) {
		// TODO Auto-generated method stub
		return session.selectList("productMapper.getFriendMainList", dto);
	}
	
	@Override
	public List<ProductDTO> getPhotoList() {
		return session.selectList("productMapper.getPhotoList");
	}

	@Override
	public ProductDTO getDetail(int pno) {
		/*HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("productNo", pno);*/
		return session.selectOne("productMapper.getDetail", pno);
	}

	@Override
	public List<ProdCommentDTO> getCommentList() {
		return session.selectList("productMapper.getCommentList");
	}

	@Override
	public List<WishListDTO> getWishList(int userNo) {
		return session.selectList("productMapper.getWishList", userNo);
	}

	@Override
	public List<WishListDTO> getFriendWishList(int userNo) {
		// TODO Auto-generated method stub
		return session.selectList("productMapper.getFriendWishList", userNo);
	}

	@Override
	public int isWish(int productNo) {
		return session.selectOne("productMapper.isWishList", productNo);
	}
	
	   @Override
	   public int getMaxPno() {
	      Integer result=session.selectOne("productMapper.getMaxPno");
		  return (result!=null&&result>0)?result:0;
	   }

}
