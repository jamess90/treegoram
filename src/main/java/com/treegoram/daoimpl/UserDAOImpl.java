package com.treegoram.daoimpl;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.treegoram.dao.UserDAO;
import com.treegoram.dto.UserDTO;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	
	@Override
	public int insertUser(UserDTO user) {
		return sqlSessionTemplate.insert("userMapper.insertUser", user);
	}

	@Override
	public UserDTO selectUser(UserDTO user) {
		return sqlSessionTemplate.selectOne("userMapper.selectUser", user);
	}

	@Override
	public int insertUserPhoto(int userNo, String userPhoto) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("userNo", userNo);
		param.put("userPhoto", userPhoto);
		
		return sqlSessionTemplate.insert("insertUserPhoto", param);
	}

	@Override
	public List<UserDTO> getPhotoList() {
		return sqlSessionTemplate.selectList("userMapper.getPhotoList");
	}
}