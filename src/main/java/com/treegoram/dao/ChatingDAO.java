package com.treegoram.dao;

import java.util.List;

import com.treegoram.dto.Friend;

public interface ChatingDAO {
	public List<Friend> getFriends(int userNo);
}
