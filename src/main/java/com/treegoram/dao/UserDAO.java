package com.treegoram.dao;

import java.util.List;

import com.treegoram.dto.UserDTO;

public interface UserDAO {
	int insertUser(UserDTO user);
	UserDTO selectUser(UserDTO user);
	int insertUserPhoto(int userNo, String userPhoto);
	List<UserDTO> getPhotoList();
}
