package com.treegoram.dao;

import java.util.List;

import com.treegoram.dto.BidListDTO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;

public interface BidListDAO {

	List<BidListDTO> getBidList(int productNo);
	int insertTender(BidListDTO dto);
	int updateTender(BidListDTO dto);
	int insertComment(ProdCommentDTO dto);
	String getRegDate(int productNo);
	List<ProdCommentDTO> getCommentList(int productNo);
	int rmWishList(WishListDTO dto);
	int addWishList(WishListDTO dto);
	int removeComment(int commentNo);
	int selectCno();
	List<ProductDTO> getInfor(int productNo);
	int promptTender(BidListDTO dto);
}
