package com.treegoram.dao;

import java.util.List;

import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;

public interface ProductDAO {
	
	int selectProductNoSeq();
	int InsertProduct(ProductDTO pdo);
	int insertTest(int no);
	int insertProductPhoto(int productNo, String photoName);
	List<ProductDTO> getMainList(ProductDTO dto);
	List<ProductDTO> getFriendMainList(ProductDTO dto);
	List<ProductDTO> getPhotoList();
	ProductDTO getDetail(int pno);
	List<ProdCommentDTO> getCommentList();
	List<WishListDTO> getWishList(int userNo);
	List<WishListDTO> getFriendWishList(int userNo);
	int isWish(int productNo);
	int getMaxPno();

}
