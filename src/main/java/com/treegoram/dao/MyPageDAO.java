package com.treegoram.dao;

import java.util.List;

import com.treegoram.dto.FindFriendDTO;
import com.treegoram.dto.FriendDTO;
import com.treegoram.dto.MyPageDTO;

public interface MyPageDAO {
	public MyPageDTO selectUserInfo(int userNo);

	public List<MyPageDTO> selectMyFriendsList(int userNo);

	public int updateUserInfo(MyPageDTO dto);

	public int deleteUser(int userNo);

	public String selectUserPassword(int userNo);

	public int deleteFriend(int userNo);

	public List<MyPageDTO> findFriend(FindFriendDTO fdto);

	public int follow(FriendDTO frienddto);

	public int unfollow(FriendDTO frienddto);
}
