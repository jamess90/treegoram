package com.treegoram.service;

import java.util.List;

import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;

public interface ProductService {

	int selectProductNoSeq();
	int InsertProduct(ProductDTO productDto);
	int insertTest(int no);
	int insertProductPhoto(int productNo, String photoName);
	List<ProductDTO> getMainList(ProductDTO dto); 
	List<ProductDTO> getFriendMainList(ProductDTO dto); 
	List<ProductDTO> getPhotoList();
	ProductDTO getDetail(int pno);
	List<ProdCommentDTO> getcommentList();
	List<WishListDTO> getWishList(int userNo);
	List<WishListDTO> getFriendWishList(int userNo);
	int isWish(int productNo);
	  int getMaxPno();
}
