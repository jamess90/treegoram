package com.treegoram.service;

import java.util.List;

import com.treegoram.dto.BidListDTO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;

public interface BidListService {
	
	List<BidListDTO> getBidList(int productNo);		// 입찰목록 가져오기
	int insertTender(BidListDTO dto);	// 입찰참여값 Insert
	int updatePrice(BidListDTO dto);	// 현재값 Update
	int insertComment(ProdCommentDTO dto);
	String getRegDate(int productNo);
	List<ProdCommentDTO> getCommentList(int productNo);
	int rmWishList(WishListDTO dto);
	int addWishList(WishListDTO dto);
	int removeComment(int commentNo);
	int selectCno();
	List<ProductDTO> getInfor(int productNo);
	int promptTender(BidListDTO dto);
}
