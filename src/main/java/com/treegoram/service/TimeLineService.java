package com.treegoram.service;

import java.util.List;

import com.treegoram.dto.TimeLineCommentDTO;
import com.treegoram.dto.TimeLineDTO;
import com.treegoram.dto.TimeLinePhoto;

public interface TimeLineService{

	int selectTimeLineSEQ();
	int insertTimeLineRegister(TimeLineDTO td);
	int insertTimeLinephoto(int timelineNo, String photoName);
	List<TimeLineDTO> selectTimeLineAll();
	String selectuserTitlebyNo(int userNo);
	int timelineDelete(int timelineNo);
	int insertTimelineComment(TimeLineCommentDTO tcd);
	List<TimeLineCommentDTO> selectCommentByTimelineNo(int timelineNo);
	String selectUserNameByUserNo(int userNo);
	List<TimeLineDTO> selectTimeLineByNo(int timelineNo);
	int updateTimelineByNo(int timelineNo, String timelineTitle,String timelineContent);
	int updatePhotoNameByNo(int timelineNo, String timelinePhotoFile);
	int deleteTimelineByNo(int timelineNo);
	int selectTimeLineNo(); 
	int deleteCommentByTcNum(int tcnum);
	List<TimeLineDTO> selectPaging(int page);
	List<TimeLinePhoto> selectPhototableBytimelineNo(int timelineNo);
	String userPhotoByuserNo(int userNo);
}
