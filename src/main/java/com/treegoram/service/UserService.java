package com.treegoram.service;

import java.util.List;

import com.treegoram.dto.UserDTO;

public interface UserService {
	int insertUser(UserDTO user);
	UserDTO selectUser(UserDTO user, String password);
	int insertUserPhoto(int userNo, String userPhoto);
	List<UserDTO> getPhotoList();
}
