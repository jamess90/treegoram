package com.treegoram.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.treegoram.dto.Friend;

public interface ChatingService {
	public List<Friend> getFriends(int userNo);
}
