package com.treegoram.service;

import java.util.List;
import javax.servlet.http.HttpSession;

import com.treegoram.dto.FindFriendDTO;
import com.treegoram.dto.FriendDTO;
import com.treegoram.dto.MyPageDTO;

public interface MyPageService {
	public MyPageDTO selectUserInfo(int userNo);

	public List<MyPageDTO> selectMyFriendsList(int userNo);

	public int updateUserInfo(MyPageDTO dto);

	public boolean deleteUser(MyPageDTO dto, HttpSession httpSession);

	public boolean deleteFriend(MyPageDTO dto, HttpSession httpSession);

	public List<MyPageDTO> findFriend(FindFriendDTO fdto);

	public int follow(FriendDTO frienddto);

	public int unfollow(FriendDTO frienddto);
}
