package com.treegoram.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treegoram.dao.ChatingDAO;
import com.treegoram.dto.Friend;
import com.treegoram.service.ChatingService;

@Service
public class ChatingServiceImpl implements ChatingService {
	@Autowired
	ChatingDAO dao;
	@Override
	public List<Friend> getFriends(int userNo) {
		return dao.getFriends(userNo);
	}

}
