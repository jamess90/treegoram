package com.treegoram.serviceImpl;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treegoram.dao.MyPageDAO;
import com.treegoram.dto.FindFriendDTO;
import com.treegoram.dto.FriendDTO;
import com.treegoram.dto.MyPageDTO;
import com.treegoram.service.MyPageService;

@Service
public class MyPageServiceImpl implements MyPageService {

	@Autowired
	MyPageDAO myPageDAO;

	@Override
	public MyPageDTO selectUserInfo(int userNo) {
		return myPageDAO.selectUserInfo(userNo);
	}

	@Override
	public List<MyPageDTO> selectMyFriendsList(int userNo) {
		return myPageDAO.selectMyFriendsList(userNo);
	}

	@Override
	public int updateUserInfo(MyPageDTO dto) {
		System.out.println("여기 수정 전 비밀번호");
		System.out.println(dto.getPassword());
		return myPageDAO.updateUserInfo(dto);
	}

	@Override
	public boolean deleteUser(MyPageDTO dto, HttpSession httpSession) {
		if (deleteFriend(dto , httpSession) == true) {
			myPageDAO.deleteUser(dto.getUserNo());
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteFriend(MyPageDTO dto, HttpSession httpSession) {
		
		String dbPassword = myPageDAO.selectUserPassword((Integer)httpSession.getAttribute("userNo"));
		if (dto.getPassword().equals(dbPassword)) {
			myPageDAO.deleteFriend(dto.getUserNo());
//			myPageDAO.deleteFriend(49);
			return true;
		}
		return false;
	}

	@Override
	public List<MyPageDTO> findFriend(FindFriendDTO fdto){
		
		return myPageDAO.findFriend(fdto);
	}

	@Override
	public int follow(FriendDTO frienddto) {
		return myPageDAO.follow(frienddto);
	}

	@Override
	public int unfollow(FriendDTO frienddto) {
		return myPageDAO.unfollow(frienddto);
	}
	
	

}
