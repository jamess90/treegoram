package com.treegoram.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treegoram.dao.TimeLineDAO;
import com.treegoram.dto.TimeLineCommentDTO;
import com.treegoram.dto.TimeLineDTO;
import com.treegoram.dto.TimeLinePhoto;
import com.treegoram.service.TimeLineService;

@Service
public class TimeLineServiceImpl implements TimeLineService{

	@Autowired
	private TimeLineDAO tdao;

	@Override
	public int selectTimeLineSEQ() {
		// TODO Auto-generated method stub
		return tdao.selectTimeLineSEQ();
	}

	@Override
	public int insertTimeLineRegister(TimeLineDTO td) {
		// TODO Auto-generated method stub
		return tdao.insertTimeLineRegister(td);
	}

	@Override
	public int insertTimeLinephoto(int timelineNo, String photoName) {
		// TODO Auto-generated method stub
		return tdao.insertTimeLinephoto(timelineNo, photoName);
	}

	@Override
	public List<TimeLineDTO> selectTimeLineAll() {
		// TODO Auto-generated method stub
		return tdao.selectTimeLineAll();
	}

	@Override
	public String selectuserTitlebyNo(int userNo) {
		// TODO Auto-generated method stub
		return tdao.selectuserTitlebyNo(userNo);
	}

	@Override
	public int timelineDelete(int timelineNo) {
		// TODO Auto-generated method stub
		return tdao.timelineDelete(timelineNo);
	}

	@Override
	public int insertTimelineComment(TimeLineCommentDTO tcd) {
		// TODO Auto-generated method stub
		return tdao.insertTimelineComment(tcd);
	}

	@Override
	public List<TimeLineCommentDTO> selectCommentByTimelineNo(int timelineNo) {
		// TODO Auto-generated method stub
		return tdao.selectCommentByTimelineNo(timelineNo);
	}

	@Override
	public String selectUserNameByUserNo(int userNo) {
		// TODO Auto-generated method stub
		return tdao.selectUserNameByUserNo(userNo);
	}

	@Override
	public List<TimeLineDTO> selectTimeLineByNo(int timelineNo) {
		// TODO Auto-generated method stub
		return tdao.selectTimeLineByNo(timelineNo);
	}

	@Override
	public int updateTimelineByNo(int timelineNo, String timelineTitle, String timelineContent) {
		// TODO Auto-generated method stub
		return tdao.updateTimelineByNo(timelineNo, timelineTitle, timelineContent);
	}

	@Override
	public int updatePhotoNameByNo(int timelineNo, String timelinePhotoFile) {
			// TODO Auto-generated method stub
		return tdao.updatePhotoNameByNo(timelineNo, timelinePhotoFile);
	}

	@Override
	public int deleteTimelineByNo(int timelineNo) {
		// TODO Auto-generated method stub
		return tdao.deleteTimelineByNo(timelineNo);
	}

	@Override
	public int selectTimeLineNo() {
		// TODO Auto-generated method stub
		return tdao.selectTimeLineNo();
	}

	@Override
	public int deleteCommentByTcNum(int tcnum) {
		// TODO Auto-generated method stub
		return tdao.deleteCommentByTcNum(tcnum);
	}

	@Override
	public List<TimeLineDTO> selectPaging(int page) {
		// TODO Auto-generated method stub
		return tdao.selectPaging(page);
	}

	@Override
	public List<TimeLinePhoto> selectPhototableBytimelineNo(int timelineNo) {
		// TODO Auto-generated method stub
		return tdao.selectPhototableBytimelineNo(timelineNo);
	}

	@Override
	public String userPhotoByuserNo(int userNo) {
		// TODO Auto-generated method stub
		return tdao.userPhotoByuserNo(userNo);
	}
	
}
