package com.treegoram.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treegoram.dao.ProductDAO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;
import com.treegoram.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductDAO productDao;

	@Override
	public int InsertProduct(ProductDTO pd) {
		// TODO Auto-generated method stub
		return productDao.InsertProduct(pd);
	}

	@Override
	public int insertTest(int no) {
		// TODO Auto-generated method stub
		return productDao.insertTest(no);
	}

	@Override
	public int selectProductNoSeq() {
		// TODO Auto-generated method stub
		return productDao.selectProductNoSeq();
	}

	@Override
	public int insertProductPhoto(int productNo, String photoName) {
		// TODO Auto-generated method stub
		return productDao.insertProductPhoto(productNo, photoName);
	}
	
	@Override
	public List<ProductDTO> getMainList(ProductDTO dto) {
		return productDao.getMainList(dto);
	}
	
	@Override
	public List<ProductDTO> getFriendMainList(ProductDTO dto) {
		// TODO Auto-generated method stub
		return productDao.getFriendMainList(dto);
	}
	
	@Override
	public List<ProductDTO> getPhotoList() {
		return productDao.getPhotoList();
	}

	@Override
	public ProductDTO getDetail(int pno) {
		return productDao.getDetail(pno);
	}

	@Override
	public List<ProdCommentDTO> getcommentList() {
		return productDao.getCommentList();
	}

	@Override
	public List<WishListDTO> getWishList(int userNo) {
		return productDao.getWishList(userNo);
	}
	
	@Override
	public List<WishListDTO> getFriendWishList(int userNo) {
		// TODO Auto-generated method stub
		return productDao.getFriendWishList(userNo);
	}
	
	
	public int isWish(int productNo) {
		int res = productDao.isWish(productNo);
		return res;
	}

	 @Override
	   public int getMaxPno() {
	      return productDao.getMaxPno();
	   }
	
}
