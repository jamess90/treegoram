package com.treegoram.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treegoram.dao.UserDAO;
import com.treegoram.dto.UserDTO;
import com.treegoram.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;
	 
	@Override
	public int insertUser(UserDTO user) {
		return userDAO.insertUser(user);
	}

	@Override
	public UserDTO selectUser(UserDTO user, String password) {
		UserDTO userinfo = userDAO.selectUser(user);
		if(userinfo.getPassword().equals(password)) {
			System.out.println("비밀번호 일치");
			System.out.println(userinfo.getUserNo());
			return userinfo;
			//return user.getUserNo();
		}
		System.out.println("비밀번호 불일치");
		return null;
	}

	@Override
	public int insertUserPhoto(int userNo, String userPhoto) {
		return userDAO.insertUserPhoto(userNo, userPhoto);
	}

	@Override
	public List<UserDTO> getPhotoList() {
		return userDAO.getPhotoList();
	}
}
