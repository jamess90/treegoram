package com.treegoram.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.treegoram.dao.BidListDAO;
import com.treegoram.dto.BidListDTO;
import com.treegoram.dto.ProdCommentDTO;
import com.treegoram.dto.ProductDTO;
import com.treegoram.dto.WishListDTO;
import com.treegoram.service.BidListService;

@Service
public class BidListServiceImpl implements BidListService{
	
	@Autowired
	private BidListDAO bidListdao;

	@Override
	public List<BidListDTO> getBidList(int productNo) {
		return bidListdao.getBidList(productNo);
	}

	@Override
	public int insertTender(BidListDTO dto) {
		return bidListdao.insertTender(dto);
	}

	@Override
	public int updatePrice(BidListDTO dto) {
		return bidListdao.updateTender(dto);
	}

	@Override
	public int insertComment(ProdCommentDTO dto) {
		return bidListdao.insertComment(dto);
	}

	@Override
	public String getRegDate(int productNo) {
		return bidListdao.getRegDate(productNo);
	}

	@Override
	public List<ProdCommentDTO> getCommentList(int productNo) {
		return bidListdao.getCommentList(productNo);
	}

	@Override
	public int rmWishList(WishListDTO dto) {
		return bidListdao.rmWishList(dto);
	}

	@Override
	public int addWishList(WishListDTO dto) {
		return bidListdao.addWishList(dto);
	}

	@Override
	public int removeComment(int commentNo) {
		// TODO Auto-generated method stub
		return bidListdao.removeComment(commentNo);
	}

	@Override
	public int selectCno() {
		return bidListdao.selectCno();
	}

	@Override
	public List<ProductDTO> getInfor(int productNo) {
		return bidListdao.getInfor(productNo);
	}

	@Override
	public int promptTender(BidListDTO dto) {
		return bidListdao.promptTender(dto);
	}
	
	
}
