var client;
var sender;
var receiver = [];
var sender_name;
var message;
var popup_count = 0;
$(document).ready(
		function() {
			$.ajax({
				method : "GET",
				url : "/treegoram/friends",
				data : {
					user_no : $('#sender_no').val()
				}
			}).done(
					function(data) {
						$.each(data, function(key, value) {
							var user_no;
							var email;
							var name;
							$.each(value, function(key, value) {
								switch (key) {
								case 'userNo':
									user_no = value;
									break;
								case 'email':
									email = value;
									break;
								case 'name':
									name = value;
									break;
								}
							});
							$('.friend_list').append(
									'<li user_email=' + email
											+ ' onclick="setSender(\''
											+ email + '\',\'' + name + '\')">'
											+ name + '(' + email + ')'
											+ '</li>');
						});
					});
			// 보내는 사람값 설정
			sender = $('#sender').val();
			sender_name = $('#sender_name').val();
			var socket = new SockJS('/treegoram/endpoint');
			client = Stomp.over(socket);
			client.connect({}, function(frame) {
				console.log('open socket');
				// 수신설정
				client.subscribe('/subscribe/' + sender, function(message) {
					$('.chat_popup').css('display', 'block');
					console.log('message', message);
					var obj = $.parseJSON(message.body);
					var html;
					// 채팅창 만들기
					var check = false;
					var indexOfReceiver;
					$.each(receiver, function(index, value) {
						if (receiver[index] == obj.sender) {
							check = true;
							indexOfReceiver = index;
						}
					});
					//새로운 채팅일경우
					if (check == false) {
						receiver[receiver.length] = obj.sender;
						indexOfReceiver = receiver.length - 1;
						addPopup(indexOfReceiver, obj.sender_name);
						$('div[data_popup=' + indexOfReceiver + '] .send')
								.click(function() {
									addSendEvent(obj.sender, indexOfReceiver)
								});
						// 채팅엔터이벤트 연결
						$(
								'div[data_popup=' + indexOfReceiver
										+ '] input[name=chat_typing]').keydown(
								function() {
									if (event.keyCode == 13)
										addSendEvent(obj.sender,
												indexOfReceiver)
								});
					}else{
						//채팅창이 숨김상태일때 이동하여 보이기
						if($('div[data_popup=' + indexOfReceiver + ']').css('display')=='none'){
						$('div[data_popup=' + indexOfReceiver + ']').css('right',
								popup_count%4*290);
						$('div[data_popup=' + indexOfReceiver + ']').attr('position',popup_count);
						}
						//채팅창이 보일때
						$('div[data_popup=' + indexOfReceiver + ']').css('z-index',
								999);
						popup_count++;
					}
					//채팅창 보이기
					$('div[data_popup=' + indexOfReceiver + ']').css('display',
							'block');
					// end of 채팅창
					$('div[data_popup=' + indexOfReceiver + '] .chat_text')
							.append(
									'<div class="receive_text"><span>'
											+ obj.sender_name + ':'
											+ obj.message + '</span></div>');
					$('.chat_text').scrollTop(
							$('.chat_text').prop('scrollHeight'));
				});
			});
		});

// 팝업 실행및 보내기 세팅
function setSender(email, sender_name) {
	global_email = email;
	var is_there = false;
	var indexOfReceiver;
	$.each(receiver, function(index, value) {
		if (receiver[index] == email) {
			is_there = true;
			indexOfReceiver = index;
		}
	});
	//새로운채팅
	if (!is_there) {
		receiver[receiver.length] = email;
		indexOfReceiver = receiver.length - 1;
		addPopup(indexOfReceiver, sender_name);
		// 보내기버튼 이벤트연결
		$('div[data_popup=' + indexOfReceiver + '] .send').click(function() {
			addSendEvent(email, indexOfReceiver)
		});
		// 채팅엔터이벤트 연결
		$('div[data_popup=' + indexOfReceiver + '] input[name=chat_typing]')
				.keydown(function() {
					if (event.keyCode == 13)
						addSendEvent(email, indexOfReceiver)
				});
	} 
	//이미 시작했던채팅
	else {
		if ($('div[data_popup=' + indexOfReceiver + ']').css('display') == 'none') {
			$('div[data_popup=' + indexOfReceiver + ']').attr('position',popup_count);
			$('div[data_popup=' + indexOfReceiver + ']').css('right', (popup_count-1)%4*290);
			popup_count ++;
		}
	}
	$('.friend_list>li').removeClass('selected');
	$('.friend_list>li[user_email=\'' + email + '\']').addClass('selected');
	$('div[data_popup=' + indexOfReceiver + ']').css('display', 'block');

}
// 멀티 팝업 추가
function addPopup(indexOfReceiver, sender_name) {
	var html = '';
	html += '<div class="chat_popup" data_popup=' + indexOfReceiver
			+ ' position='+popup_count+' style="right:' + (290 * (popup_count % 4)) + 'px">';
	html += '<div class="target_man" >' + sender_name
			+ '와(과) 채팅</div><div class="chat_exit">X</div>';
	html += '<div class="chat_text"></div>';
	html += '<div class="chat_sendbar">';
	html += '<input type="hidden" name="receiver" id="receiver" >';
	html += '<input type="text" name="chat_typing" />';
	html += '<input class="send" type="button" value="보내기" />';
	html += '</div></div>';
	$('#sidebar-right').append(html);
	$('div[data_popup=' + indexOfReceiver + ']>.chat_exit').click(function() {
		$('div[data_popup=' + indexOfReceiver + ']').css('display', 'none');
		var p=$('div[data_popup=' + indexOfReceiver + ']').attr('position');
		for (i = parseInt(p); i < popup_count-1; i++) {
			$('div[position=\'' + (i+1) + '\']').css("right", (((i) % 4) * 290));
			$('div[position=' + (i+1) + ']').attr('position',i);
		}
		popup_count--;
	});
	popup_count++;
}

function addSendEvent(email, indexOfReceiver) {
	message = $(
			'div[data_popup=' + indexOfReceiver + '] input[name=chat_typing]')
			.val();
	var data = {
		'sender' : sender,
		'sender_name' : sender_name,
		'receiver' : email,
		'receiver_name' : receiver[indexOfReceiver],
		'message' : message
	};
	client.send('/app/echo/' + email, {}, JSON.stringify(data));
	$('div[data_popup=' + indexOfReceiver + '] .chat_text').append(
			'<div class="send_text"><span>나:' + message + '</span></div>');
	$('div[data_popup=' + indexOfReceiver + '] .chat_text').scrollTop(
			$('.chat_text').prop('scrollHeight'));
}
