<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
	<head>
	<script src="plugins/jquery/jquery.min.js"></script>
	<link href="plugins/bootstrap/bootstrap.css" rel="stylesheet">

	<style type="text/css">
	</style>
</head>
<body>
<input type="hidden" id="raiseUnit" name="raiseUnit" value="${pdto.raiseUnit }" />
<input type="hidden" id="promptPrice" name="promptPrice" value="${pdto.promptPrice }" />
<input type="hidden" id="startPrice" name="startPrice" value="${pdto.startPrice }" />
    <table class="table">
    	<tr>
    		<td><b style="font-size: 20px; font-weight: bold; margin-top: 10px;">${sessionScope.userName }</b></td>
  			<td><input type="number" step="${pdto.raiseUnit }" id="bidPrice" name="bidPrice" placeholder="입찰금액을 입력하세요"/></td>
  			<td><input type="button" class="btn btn-default" id="joinBid" value="입찰" /></td>
    	</tr>
    	<tr>
    		<th>상품명</th>
    		<th>입찰자</th>
    		<th>가격</th>
    	</tr>
    	<tr>
    		<td>START</td>
    		<td>START</td>
    		<td>${pdto.startPrice }</td>
    	<div id="addTenderList"></div>
    	<c:forEach var="dto" items="${bidList }">
    	<tr>
    		<td><b style="font-weight: bold;">${pdto.productName }</td>
    		<td>${dto.userName }</td>
    		<td>${dto.bidPrice }</td>
    	</tr>
    	<input type="hidden" id="userNo" name="userNo" value="${dto.userNo }" />
    	<input type="hidden" id="userName" name="userName" value="${dto.userName }" />
    	</c:forEach>
    	<input type="hidden" id="productNo" name="productNo" value="${productNo }" />
	</table>

</body>
<script>
	$(document).ready(function () {
		var pno = $('#productNo').val();
		var userno = '${sessionScope.userNo}';		
		var bidprice;
		var userName = '${sessionScope.userName}';	
		// 입찰 클릭 시 발생 이벤트
		$('#joinBid').click(function() {
			bidprice = $('#bidPrice').val();
			raiseUnit = $('#raiseUnit').val();
			promptPrice = $('#promptPrice').val();
			startPrice = $('#startPrice').val();

			if((startPrice-bidprice) >= 0) {
				alert('시작가보다 높은 가격으로 입찰을 시도해보세요.');
			} else { 
			if((bidprice-startPrice)%raiseUnit == 0) {			// 입찰가가 입찰단위에 맞는지 확인
				if(bidprice >= promptPrice) {					// 즉시구매가가 입찰되었거나 더 높은 가격이 입찰되었을 경우
					$.post("<c:url value='/promptTender.do' />", {"userNo" : userno, "productNo" : pno, "bidPrice" : bidprice},
					function(list) {
						alert('즉시구매를 완료하였습니다.');
						var dto;
						var str = '';
						for(var i=0; i<list.length; i++) {
							dto = list[i];
						}
						
						$(opener.document).find('#product' + pno).hide();
						window.close();
					});
				} else {
			// 부모창의 current_price 를 변경시켜줌 / 그리고 입찰추가
			$.post("<c:url value='/tender.do' />", {"userName" : userName, "userNo" : userno, "productNo" : pno, "bidPrice" : bidprice}, 
			function(list) {
				var dto;
				var str = '';
				for(var i=0; i<list.length; i++) {
					dto = list[i];
				}
				str += '<tr><td>' + dto.productNo + '</td>';
				str += '<td>' + dto.userName + '</td>';
				str += '<td>' + dto.bidPrice + '</td></tr>';
				$('#addTenderList').append(str);
				$(opener.document).find('#current_Price${productNo }').empty();
				$(opener.document).find('#current_Price${productNo }').append(dto.bidPrice);
				alert("입찰에 참여하셨습니다.");
				window.close();
			}); 
				}
		}// if() end
		else {
			alert('입찰 단위는 ' + raiseUnit + '원 입니다.');
		}
		} // 시작가보다 작은 입찰가 if() end
	}); // 입찰 버튼 클릭 function() end
});
</script>
</html>
