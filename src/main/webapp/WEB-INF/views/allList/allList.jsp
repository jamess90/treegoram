<%@page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html lang="en">
	<head>
	<style type="text/css">

nav{
	margin-top:3%;
	line-height: 50px;
	background-color:#5D5D5D;
	padding: 15px;
	min-height:2000px;
	width:200px;
	float:left;
}
.bod{
    padding: 15px;
    min-height: 2100px;
    float: left;
    width: 67%;
    background: white;
}
.chat{
    padding: 15px;
    min-height: 2100px;
    float: right;
    width: 19.8%;
    background: #4C4C4C;
}
footer{
	background-color:#5D5D5D;
	padding-top:20px;
	text-align:center;
	height:50px;
	clear:both;
	color: white;
}
	</style>
		<meta charset="utf-8">
		<title>TreeGoram</title>
		<meta name="description" content="description">
		<meta name="author" content="DevOOPS">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="../plugins/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="../plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="../plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
		<link href="../plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
		<link href="../plugins/xcharts/xcharts.min.css" rel="stylesheet">
		<link href="../plugins/select2/select2.css" rel="stylesheet">
		<link href="../plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
		<link href="../css/style_v2.css" rel="stylesheet">
		<link href="../plugins/chartist/chartist.min.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <!-- <link href="/controller/css/aa.css" rel="stylesheet" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> <!-- 1/25 추가 -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.min.js"></script>
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
<body>


<!--Start Header-->
<%@ include file="../main/main_header.jsp" %>
<!--End Header-->

<nav>
<%@ include file="../main/main_sidebar.jsp" %>
</nav>
<!-- 바디부분 작성시작 -->
<section class="body">
<div class="row">
  <div class="col-md-4">.col-md-4</div>
  <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
</div>
<div class="row">
  <div class="col-md-3 col-md-offset-3">.col-md-3 .col-md-offset-3</div>
  <div class="col-md-3 col-md-offset-3">.col-md-3 .col-md-offset-3</div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3">.col-md-6 .col-md-offset-3</div>
</div>

</section>
<!-- 바디 부분 작성 끝 -->
<!-- chat section을 써도 되고 안써도 되고.  -->
<%-- <section class="chat">
<%@ include file="main_chat.jsp" %>
</section> --%>

<footer>
<%@ include file="../main/main_footer.jsp" %>
</footer>

<%-- <!--Start Container-->
<div id="main" class="container-fluid">
<div class="row">

<!-- main side bar  -->
<%@ include file="main_sidebar.jsp" %>		

<!--main chatting bar  -->
<%@ include file="main_chattingbar.jsp" %>
		
<!--main body-->
<%@ include file="main_body.jsp" %>
</div>
</div> --%>

<!--End Container-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="http://code.jquery.com/jquery.js"></script>-->
<script src="../plugins/jquery/jquery.min.js"></script>
<script src="../plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../plugins/bootstrap/bootstrap.min.js"></script>
<script src="../plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
<script src="../plugins/tinymce/tinymce.min.js"></script>
<script src="../plugins/tinymce/jquery.tinymce.min.js"></script>
<!-- All functions for this theme + document.ready processing -->
<script src="../js/devoops.js"></script>
</body>
</html>
