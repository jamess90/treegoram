select * from user_list;
select * from product;
delete from product where product_no between 9 AND 12;
delete from product_photo where product_no between 9 AND 12;
select * from product_photo;
-- 전체 테이블명 확인하기
select table_name from user_tables;
-- bid_list 테이블 확인
select * from bid_list;
select * from wish_list;
select * from product_comment;
select * from product
where end_time > sysdate;

select product_comment_SEQ.nextval from dual
-- 관심목록 추가
insert into wish_list(user_no, product_no)
values(21, 21);

insert into bid_list(product_no, user_no, bid_price, bid_success)
values(17, 1, 3500, 'N');
--
select * from user_list;
select * from product_comment;
SELECT email, password, user_no as userNo, user_name as userName
	FROM user_list 
	WHERE email = 'zoflrjs2145@naver.com'

select product_comment_SEQ.nextval from dual

drop sequence product_SEQ;
drop trigger product_product_no_AI_TRG
drop sequence product_comment_SEQ;
drop trigger product_comment_AI_TRG;
drop sequence order_list_SEQ;
drop trigger order_list_order_no_AI_TRG;

drop table product_photo;
drop table product_comment;
drop table wish_list;
drop table bid_list;
drop table order_list;
drop table product;



drop sequence product_comment_SEQ;
drop trigger product_comment_comment_AI_TRG;
select product_no as productNo, product_name as productName, category, start_price as startPrice,
raise_unit as raiseUnit, current_price as currentPrice, text,
           like_number as likeNumber, user_no as userNo, start_time as startTime, end_time as endTime
    from product
    order by product_no DESC;

select * from product_comment;
insert into product(product_name, category, start_price, raise_unit, current_price, text,
                    like_number, user_no, start_time, end_time)
values('KFC 버거', 'etc', 500, 100, 500, '징거버거 이모티콘 팜', 0, 1, sysdate, sysdate+3);

select * from product;
select product_no as productNo, comment_no as commentNo, user_no as userNo, text, reg_date as regDate
	from product_comment
	order by product_no DESC
-- 댓글 추가
insert into product_comment(product_no, user_no, text, reg_date)
values(17, 1, '감사합니다.', sysdate);

select product_no, user_no, bid_price, bid_success
from bid_list;

-- 경매목록 (이름으로 가져가기)
select b.product_no, u.user_name, b.bid_price, b.bid_success
from bid_list b, user_list u
where b.product_no=17 AND b.user_no = u.user_no;

select b.product_no as productNo, u.user_name as userName, b.bid_price as bidPrice, b.bid_success as bidSuccess
from bid_list b, user_list u
where b.user_no = u.user_no AND b.product_no=18
order by bid_price DESC;


select *
    from (
    		select * from product
    		order by product_no DESC
    	 )
    where rownum between 6 AND 10
-- 페이징
select * from product order by product_no desc;
select *
	from (
		select *
		from product
		order by product_no desc
		 )
	where rownum between 3 AND 5;		-- (5x0)+1 and (5x1);	1~5
										-- (5x1)+1 and (5x2);	6~10
select * from product;
update product set prompt_price=60000
where product_no = 47
	select *
from
	(
		select rownum as rnum, p.*
		from (
			select *
			from product
			order by product_no desc
		     ) p
		)
	where rnum between 6 AND 10;
	
	----------------------------------------------------
	select product_no as productNo, product_name as productName, category, start_price as startPrice,
	       raise_unit as raiseUnit, current_price as currentPrice, text, user_name as userName,
           like_number as likeNumber, prompt_price as promptPrice, user_no as userNo, start_time as startTime, end_time as endDate
    from
	(
		select rownum as rnum, p.*
		from (
			select pro.*, u.user_name
			from product pro, user_list u
			where pro.user_no = u.user_no AND category='funiture'
			order by product_no desc
		     ) p
		) 
	where rnum between 1 AND 5
	
	select * from wish_list;
	select * from user_list;
	select * from product;
	select count(*)
from product p, wish_list w
where p.product_no = w.product_no AND w.product_no = 19

ALTER TABLE user_list RENAME COLUMN user_photo_file to user_photo;

update user_list set user_photo = 'profile_photo.jpg'
where user_no=21

select * from product_comment;
select *
from (
select p.comment_no as commentNo, p.product_no as productNo, p.user_no as userNo, p.text, p.reg_date as regDate, u.user_name as userName
	from product_comment p, user_list u
	where p.user_no = u.user_no AND product_no=#{productNo}
	order by comment_no DESC
	) a
order by a.product_no desc;

	