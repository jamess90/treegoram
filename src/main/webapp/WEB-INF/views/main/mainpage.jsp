<%@page contentType="text/html; charset=UTF-8"%>

<html>
<head>
<meta charset="UTF-8">
<title>Title of the document</title>
</head>
<!--Start Header-->
<%@ include file="main_header.jsp"%>
<!--End Header-->

<%@ include file="main_sidebar.jsp"%>

<section class="bod">
	<%@ include file="main_body.jsp"%>
</section>
<%@ include file="main_footer.jsp"%>
</body>
</html>