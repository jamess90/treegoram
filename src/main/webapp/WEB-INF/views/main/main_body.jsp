<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet" href="css/flipclock.css">
<!--  남은시간 countdown용 -->
<script src="js/flipclock.js"></script>
<!--  남은시간 countdown용 -->

<div class="row">
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=wear"
			style="color: #000000;"><img src="img/wear.png" /></a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=furniture"
			style="color: #000000;"><img src="img/lifey.png" /></a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=life"
			style="color: #000000;"><img src="img/furniture.png" /></a>
	</div>
	<div class="col-xs-2 text-center">
		<a href="${pageContext.request.contextPath}/mainpage.do?category=it"
			style="color: #000000;"><img src="img/it.png" /></a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=sports"
			style="color: #000000;"><img src="img/sports2.png" /></a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=etc"
			style="color: #000000;"><img src="img/etc.png" /></a>
	</div>
</div>
<div class="row">
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=wear"
			style="font-size: 13px; color: #ff0000; font-weight: bold;">WEAR</a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=furniture"
			style="font-size: 13px; color: #ff0000; font-weight: bold;">LIFE</a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=life"
			style="font-size: 13px; color: #ff0000; font-weight: bold;">FURNITURE</a>
	</div>
	<div class="col-xs-2 text-center">
		<a href="${pageContext.request.contextPath}/mainpage.do?category=it"
			style="font-size: 13px; color: #ff0000; font-weight: bold;">IT</a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=sports"
			style="font-size: 13px; color: #ff0000; font-weight: bold;">SPORTS</a>
	</div>
	<div class="col-xs-2 text-center">
		<a
			href="${pageContext.request.contextPath}/mainpage.do?category=etc"
			style="font-size: 13px; color: #ff0000; font-weight: bold;">ETC</a>
	</div>
</div>
<c:choose>
	<c:when test="${mainList != null }">
		<c:forEach var="dto" items="${mainList }">
			<div class="panel panel-default panel-table"
				id="product${dto.productNo }" style="margin-top: 15px;">
				<div class="panel-heading" style="background-color: #ffffff;">
					<div class="row">
						<div class="col col-xs-1 text-right">
							<img
								src="<c:url value="/uploadImg/${dto.userPhoto}"/>"
								style="width: 50px; height: 50px;" />
						</div>
						<div class="col col-xs-5 text-left" style="padding-top: 7px;">
							<a href="#"><b>${dto.userName }</b></a>
							<p>${dto.startTime.substring(0, 15) }</p>
						</div>

						<div class="col col-xs-6 text-right" style="height: 49.78px;">


							<div class="remainingTime">
								<div class="clock${dto.productNo }" style="padding-left: 100px;"></div>
								<div class="message"></div>
							</div>

						</div>

					</div>
				</div>
				<div class="panel-body">

					<!--  ${wish_list} forEach로 돌리고 값이 있는지 없는지 가져와서 표시하기 -->
					<input type="hidden" name="productNo${dto.productNo }"
						value="${dto.productNo }" /> <input type="hidden"
						name="userNo${dto.productNo }" value="${dto.userNo }" /> <input
						type="hidden" name="currentPrice${dto.productNo }"
						value="${dto.currentPrice }" /> <input type="hidden"
						name="raiseUnit${dto.productNo }" value="${dto.raiseUnit }" /> <input
						type="hidden" name="startTime${dto.productNo }"
						value="${dto.startTime }" /> <input type="hidden"
						name="endDate${dto.productNo }" value="${dto.endDate }" /> <input
						type="hidden" name="promptPrice${dto.productNo }"
						value="${dto.promptPrice }" /> <input type="hidden" name="maxPno"
						id="maxPno" value="${maxPno }" /> ${dto.text }

					<div class="row" style="text-align: center;">
						<br>
						<c:forEach var="photodto" items="${photoList }">
							<c:choose>
								<c:when test="${photodto.productNo == dto.productNo }">
									<div class="col-xs-4 text-center">
										<img
											src="<c:url value="/uploadImg/${photodto.productPhoto}"/>"
											style="height: 200px;" />
									</div>
								</c:when>
							</c:choose>
						</c:forEach>
					</div>

				</div>
				<div class="panel-footer" style="background-color: #ffffff;">
					<div class="row">
						<div class="col col-xs-3 text-center"
							onclick="return addWishList(${dto.productNo});">
							<c:if test="${dto.isWish == 1}">
								<div id="wishRight${dto.productNo }" style="margin-top: 10px;">
									<img src="img/heart.png" width="20px" height="20px" />wishList
								</div>
								<div id="wishNot${dto.productNo }"
									style="display: none; margin-top: 10px;">
									<img src="img/heart-o.png" width="20px" height="20px" />wishList
								</div>
							</c:if>
							<c:if test="${dto.isWish == 0}">
								<div id="wishRight${dto.productNo }"
									style="display: none; margin-top: 10px;">
									<img src="img/heart.png" width="20px" height="20px" />wishList
								</div>
								<div id="wishNot${dto.productNo }" style="margin-top: 10px;">
									<img src="img/heart-o.png" width="20px" height="20px" />wishList
								</div>
							</c:if>
						</div>
						<div class="col col-xs-3 text-center"
							onclick="return showComment(${dto.productNo});">
							<i class="fa fa-comment" style="margin-top: 10px;"> 댓글</i>
						</div>
						<div class="col col-xs-3 text-center"
							onclick="return joinTender(${dto.productNo});">
							<i class="fa fa-money" style="margin-top: 10px;"> 입찰</i>
						</div>
						<div class="col col-xs-3 text-center">
							<b id="current_Price${dto.productNo }"
								style="font-weight: bold; color: #ff0000; font-size: 25px;">${dto.currentPrice }
							</b>원
						</div>
					</div>
				</div>
				<div class="panel-footer" style="background-color: #ffffff;">
					<div class="row">
						<div class="col col-xs-2 text-center" style="margin-top: 15px;">
							<b id="replyName${dto.productNo }">${sessionScope.userName }</b>
						</div>
						<div class="col col-xs-8 text-left">
							<textarea id="commentText${dto.productNo }" class="autosize"
								style="resize: none; width: 700px;" placeholder="댓글을 입력해보세요."></textarea>
						</div>
						<div class="col col-xs-2 text-center">
							<button class="btn btn-default"
								onclick="return addComments(${dto.productNo });">등 록</button>
						</div>
					</div>
				</div>
				<div class="panel-footer"
					style="background-color: #ffffff; display: none;"
					id="comment${dto.productNo }">
					<!-- 댓글 목록 보여주기 -->

					<c:forEach var="commentDTO" items="${commentList }">
						<c:choose>
							<c:when test="${commentDTO.productNo == dto.productNo}">
								<div class="row" id="removecomment${commentDTO.commentNo }">
									<div class="col col-xs-2 text-center">
										<b style="margin-top: 10px;">${commentDTO.userName }</b>
									</div>
									<div class="col col-xs-8 text-left">
										${commentDTO.text }
										<c:if test="${commentDTO.userNo == sessionScope.userNo }">
											<i class="glyphicon glyphicon-remove"
												onclick="return 
('${commentDTO.commentNo }');"></i>
										</c:if>
									</div>

									<div class="col col-xs-2 text-center">
										${commentDTO.regDate.substring(0, 15) }</div>
								</div>
							</c:when>
						</c:choose>
					</c:forEach>
					<div id="addcommentPage${dto.productNo }"></div>
				</div>
			</div>
		</c:forEach>
	</c:when>
</c:choose>
<div id="addPage"></div>

<script>
 // 입찰 이벤트, 댓글, 남은시간<div id="remainingTime">에 표시, 
 // 입찰 이벤트
    
   
 // 남은 시간 표시
 var clock;
 var now = new Date();
 var startTime;
 var endTime;
// 남은시간 표시 end
 var pageNumber = 0;
 var endPageNumber = 1;
 var maxPno = $('#maxPno').val();
 var scrollmaxPno = $('#maxPno').val();
 var raisePage = 5;
// 위시리스트에 추가하기
function addWishList(pno) {
   var userNo = '${sessionScope.userNo}';
   if ($("#wishNot" + pno).css("display") == "none") {
      $.post("<c:url value='/rmWishList.do' />", {"userNo" : userNo, "productNo" : pno}, function(result) {
         $("#wishRight" + pno).hide();   
         $("#wishNot" + pno).show();   
      });
       
    } // 위시리스트에 이미 추가되어있는 경우
    else {
       $.post("<c:url value='/addWishList.do' />", {"userNo" : userNo, "productNo" : pno}, function(result) {
         $("#wishNot" + pno).hide();
         $("#wishRight" + pno).show(); 
         alert("찜목록에 추가되었습니다.");
       });
    } // 위시리스트에 추가하는 경우
   /* alert('pno: ' + pno);
   var name = '${sessionScope.userName }';
   
   $.post("<c:url value='/addWishList.do' />", {"productNo" : pno, "userName" : name}, function()) */
}
//댓글 삭제 이벤트
function rmComment(cno) {
   var commentNo = cno;
   $.post("<c:url value='/rmComment.do' />", {"commentNo" : commentNo}, function(result) {
      
      $('#removecomment' + cno).hide();
      alert('댓글 삭제가 완료되었습니다.');
   });
}

//댓글 추가 이벤트
function addComments(pno) {
   var name = '${sessionScope.userName }';   // 세션값으로 넣으면 됨(일단 넣음)
   var content = $('#commentText' + pno).val();
   var userNo = ${sessionScope.userNo };
   if(content.length == 0) {
      alert('내용을 입력해주세요.');
   } else {
   $.post("<c:url value='/addComment.do' />", {"userName" : name, "text" : content, "productNo" : pno, "userNo" : userNo}, function(data) {
      var regDate = data.regDate;
      var str = '';
      str += '<div class="row" id="removecomment' + data.commentNo + '">';
        str += '<div class="col col-xs-2 text-center">';
        str += '<b style="margin-top: 10px;">' + name + '</b>';
        str += '</div>';
        str += '<div class="col col-xs-8 text-left">';
        str += content;
        str += " <i class='glyphicon glyphicon-remove' onclick='return rmComment(" + data.commentNo + ");'></i>";
        str += '</div>';
        str += '<div class="col col-xs-2 text-center">';
        str += regDate.substring(0, 15);
        str += '</div>';
        str += '</div>';
        $('#commentText' + pno).val('');
        $('#addcommentPage'+pno).append(str);
        alert('댓글 등록이 완료되었습니다.');
   });
   }// if else() end
}
//댓글 이벤트
function showComment(pno) {
    if ($("#comment" + pno).css("display") == "none")
      $("#comment" + pno).show();
    else
      $("#comment" + pno).hide();
}
//입찰 이벤트
function joinTender(pno) {
   // var sessionId = id;
   var sx = parseInt(screen.width);
   var sy = parseInt(screen.height);
   var x = (sx / 2) + 50;
   var y = (sy / 2) - 25;
   
   var win = window.open("${pageContext.request.contextPath}/tender.do?productNo=" + pno,
         "calendarwin", "resizable=0, width=500, height=390");      
   win.moveTo(x, y);
   
};
 $(document).ready(function() {
   		
      // 남은 시간 표시
      for(var i=maxPno; i>0; i--) {
         var flag = $('input[name=endDate' + i + ']').val();
         if(flag != null) {
         clock = $('.clock'+i).FlipClock({
           clockFace: 'DailyCounter',
           autoStart: false,
           callbacks: {
              stop: function() {
                 $('.message').html('The clock has stopped!')
              }
           }
       });

      endTime = $('input[name=endDate' + i +']').val();
      endTimeDate = endTime.substring(0, 10);
      
      year = now.getYear();
      month = now.getMonth();
      day = now.getDate();
      var nowDate = new Date(year, (month), day);
      var endDate = new Date(endTimeDate);
      var btMs = (endDate.getTime()+86399000) - now.getTime();
      var btDay = btMs / 1000;
      //alert('nowDate: ' + nowDate + ', endDate: ' + endDate + ', btMs: ' + btMs + ', btDay: ' + btDay);
      // startTime = startTimeMonth + startTimeDay + startTimeHour + startTimeMin + startTimeSec;
      // endTimeSet = endTimeHour + endTimeMin + endTimeSec;
       clock.setTime(btDay);
       clock.setCountdown(true);
       clock.start();      // 시간 countdown 시작
         }
      }
    // 1, 5개의 칼럼이 나온다는 얘기
    $(document).scroll(function() {
         
         var documentHeight = $(document).height();                  // HTML Document 높이
         var scrollHeight = $(window).scrollTop() + $(window).height();   // 현재 스크롤값을 반환 + 브라우저 viewPort높이 = scorllHeight

         $.post("<c:url value='/addphotoList.do' />", function(photoList) {
         if(pageNumber < endPageNumber) {                        // 현재 페이징넘버가 마지막 페이징 번호보다 작은지 확인
            if(documentHeight-0.5 < scrollHeight) {                  // scroll의 높이와 document의 높이가 같은지 확인
                  pageNumber += 1;
               endPageNumber += 1;
                $.post("<c:url value='/addMainList.do' />", {"pageNo" : pageNumber, "endpageNo" : endPageNumber}, function(data) {

                  var produtNo;
                  for(var cnt=0; cnt<data.length; cnt++) {
                     var dto = data[cnt];
                     productNo = dto.productNo;
                     var str = '';
                     str += '<div class="panel panel-default panel-table" style="margin-top: 15px;">';
                     str += '<div class="panel-heading" style="background-color: #ffffff;">';
                         str += '<div class="row">';
                         str += '<div class="col col-xs-1 text-right">';
                         
                         str += '<img src=/uploadImg/' + dto.userPhoto + ' style="width: 50px; height: 50px;"/>';
                         str += '</div>';
                         str += '<div class="col col-xs-5 text-left" style="padding-top: 7px;">';
                         str += '<a href="#"><b>' + dto.userName + '</b></a>';  
                         str += '<p> ' + dto.startTime.substring(0, 15) + '</p>';
                         str += '</div>';
                         str += '<div class="col col-xs-6 text-right" style="height: 49.78px;">';
                         str += '<div class="remainingTime">';
                         str += '<div class="clock' + dto.productNo + '" style="padding-left: 100px;"></div>';
                         str += '<div class="message"></div>';
                         str += '</div>';
                         str += '</div>';
                         str += '</div>';
                         str += '</div>';
                         
                         str += '<div class="panel-body">';
                         str += '<input type="hidden" name="productNo' + dto.productNo + '" value="' + dto.productNo + '" />';
                         str += '<input type="hidden" name="userNo' + dto.productNo + '" value="' + dto.userNo + '" />';
                         str += '<input type="hidden" name="currentPrice' + dto.productNo + '" value="' + dto.currentPrice + '" />';
                         str += '<input type="hidden" name="raiseUnit' + dto.productNo + '" value="' + dto.raiseUnit + '" />';
                         str += '<input type="hidden" name="startTime' + dto.productNo + '" value="' + dto.startTime + '" />';
                         str += '<input type="hidden" name="endDate' + dto.productNo + '" value="' + dto.endDate + '" />';
                         str += '<input type="hidden" name="promptPrice' + dto.productNo + '" value="' + dto.promptPrice + '" />';  
                         str += dto.text;
                         
                         str += '<div class="row">';
                           for(var i=0; i<photoList.length; i++) {
                              var photodto = photoList[i];
                              if(photodto.productNo == dto.productNo) {
                                  str += '<div class="col-xs-4 text-center">';
                                str += '<img src="<c:url value="/uploadImg/' + photodto.productPhoto + '"/>" style="height: 200px;"/>';
                                str += '</div>';
                              }
                           }
                           str += '</div>';
                           str += '</div>';
                           
                           str += '<div class="panel-footer" style="background-color: #ffffff;">';
                         str += '<div class="row">';
                         str += '<div class="col col-xs-3 text-center" onclick="return addWishList(' + dto.productNo + ');">';
                          if(dto.isWish==1) {
                          str += '<div id="wishRight' + dto.productNo + '" style="margin-top: 10px;"><img src="img/heart.png" width="20px" height="20px" />wishList</div>';
                          str += '<div id="wishNot' + dto.productNo + '" style="display: none; margin-top: 10px;"><img src="img/heart-o.png" width="20px" height="20px" />wishList</div>';
                          }
                          if(dto.isWish==0) {
                          str += '<div id="wishRight' + dto.productNo + '" style="display: none; margin-top: 10px;"><img src="img/heart.png" width="20px" height="20px" />wishList</div>';
                          str += '<div id="wishNot' + dto.productNo + '" style="margin-top: 10px;"><img src="img/heart-o.png" width="20px" height="20px" />wishList</div>';
                          }
                           str += '</div>';
                         
                         str += '<div class="col col-xs-3 text-center" onclick="return showComment(' + dto.productNo + ');">';
                         str += '<i class="fa fa-comment" style="margin-top: 10px;"> 댓글 </i>';
                         str += '</div>';
                         str += '<div class="col col-xs-3 text-center" onclick="return joinTender(' + dto.productNo + ');">';
                         str += '<i class="fa fa-money" style="margin-top: 10px;">입찰</i>';
                         str += '</div>';
                         str += '<div class="col col-xs-3 text-center">';
                         str += '<b id="current_Price' + dto.productNo + '" style="font-weight:bold; color: #ff0000; font-size: 25px;">' + dto.currentPrice + '</b>원';
                         str += '</div>';
                         str += '</div>';
                         str += '</div>';
                         
                         str += '<div class="panel-footer" style="background-color: #ffffff;">';
                         str += '<div class="row">';
                         str += '<div class="col col-xs-2 text-center" style="margin-top: 15px;">';
                         str += '<b id="replyName">${sessionScope.userName}' + '</b>';   
                         str += '</div>';
                         str += '<div class="col col-xs-8 text-left">';
                          str += '<textarea id="commentText' + dto.productNo + '" class="autosize" style="resize:none; width: 700px;" placeholder="댓글을 입력해보세요."></textarea>';
                          str += '</div>';
                          str += '<div class="col col-xs-2 text-center">';
                          str += '<button class="btn btn-default" onclick="return addComments(' + dto.productNo + ');">등 록</button>';
                          str += '</div>';
                          str += '</div>';
                          str += '</div>';
                       
                         str += '<div class="panel-footer" style="background-color: #ffffff; display: none;" id="comment' + dto.productNo + '">';
                         str += '<div id="addcommentPage' + dto.productNo + '"></div>';
                     $.post("<c:url value='/getComment.do' />", {"productNo" : productNo}, function(commentList) {
                        for(var cmCnt=0; cmCnt<commentList.length; cmCnt++) {
                           var dto = commentList[cmCnt];
                           str += '<div class="row" id="removecomment"' + dto.commentNo + '>';
                           str += '<div class="col col-xs-2 text-center">';
                           str += '<b style="margin-top: 10px;">' + dto.userName + '</b>';
                           str += '</div>';
                           str += '<div class="col col-xs-8 text-left">' + dto.text;
                           if(dto.userNo == '${sessionScope.userNo}') {
                              str += '<i class="glyphicon glyphicon-remove" onclick="return rmComment(' + dto.commentNo  +');"></i>';
                           }
                           str += '</div>';
                           str += '<div class="col col-xs-2 text-center">' + dto.regDate.substring(0, 15) + '</div>';
                           str += '</div>';
                        }
                     });
                     str += '</div>';
                     
                         str += '</div>';
                         $('#addPage').append(str);
                  }
               }); // addMainList.do.post end
                // 남은 시간 표시
               
                for(var i=maxPno; i>0; i--) {
         var flag = $('input[name=endDate' + i + ']').val();
         if(flag != null) {
         clock = $('.clock'+i).FlipClock({
           clockFace: 'DailyCounter',
           autoStart: false,
           callbacks: {
              stop: function() {
                 $('.message').html('The clock has stopped!')
              }
           }
       });

      endTime = $('input[name=endDate' + i +']').val();
      endTimeDate = endTime.substring(0, 10);
      
      year = now.getYear();
      month = now.getMonth();
      day = now.getDate();
      var nowDate = new Date(year, (month), day);
      var endDate = new Date(endTimeDate);
      var btMs = (endDate.getTime()+86399000) - now.getTime();
      var btDay = btMs / 1000;

       clock.setTime(btDay);
       clock.setCountdown(true);
       clock.start();      // 시간 countdown 시작
         }
      }
                
            }
         }
         }); // photoList.post end
         
       });
 });

 </script>
