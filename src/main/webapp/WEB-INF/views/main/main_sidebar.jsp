<%@page contentType="text/html; charset=UTF-8"%>
<nav>
	<ul class="nav main-menu">
		<li class="dropdown"><a href="/treegoram/mainpage.do" class="link"> <i
				class="fa fa-bar-chart-o"></i> <span class="hidden-xs">전체경매목록</span>
		</a></li>
		<li class="dropdown"><a class="link" href="/treegoram/friendProductPage.do">
				<i class="fa fa-desktop"></i> <span class="hidden-xs">친구경매목록</span>
		</a></li>
		<li class="dropdown"><a class="link" href="/treegoram/timelinepage">
				<i class="fa fa-desktop"></i> <span class="hidden-xs">타임라인작성</span>
		</a></li>
		<li class="dropdown"><a class="link" href="/treegoram/friendSearchPage.do?searchWord=">
				<i class="fa fa-desktop"></i> <span class="hidden-xs">친구 검색</span>
		</a></li>
		<li class="dropdown"><a class="link" href="/treegoram/myInfo.do">
				<i class="fa fa-desktop"></i> <span class="hidden-xs">내 정보</span>
		</a></li>
		<li><a class="link" href="/treegoram/logout.do"> 
				<i class="fa fa-dashboard"></i> <span class="hidden-xs">로그아웃</span>
		</a></li>
	</ul>
</nav>