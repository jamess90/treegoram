<%@page contentType="text/html; charset=UTF-8"%>
<head>
<meta charset="utf-8">
<title>TreeGoram</title>
<meta name="description" content="description">
<meta name="author" content="DevOOPS">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="plugins/bootstrap/bootstrap.css" rel="stylesheet">
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Righteous'
	rel='stylesheet' type='text/css'>
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="plugins/xcharts/xcharts.min.css" rel="stylesheet">
<link href="plugins/select2/select2.css" rel="stylesheet">
<link href="plugins/justified-gallery/justifiedGallery.css"
	rel="stylesheet">
<link href="css/style_v2.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="plugins/chartist/chartist.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
					<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
					<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
			<![endif]-->
<!--End Container-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="http://code.jquery.com/jquery.js"></script>-->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
<script src="plugins/tinymce/tinymce.min.js"></script>
<script src="plugins/tinymce/jquery.tinymce.min.js"></script>
<!-- All functions for this theme + document.ready processing -->
<script src="js/devoops.js"></script>
<script type="text/javascript" src="js/sockjs.min.js"></script>
<script type="text/javascript" src="js/stomp.js"></script>
<script type="text/javascript" src="js/chat.js"></script>
</head>
<body>
	<div class="header_wrap">
		<header class="navbar">
			<div class="container-fluid expanded-panel">
				<div class="row">
					<div id="logo" class="col-xs-12 col-sm-2">
						<a href="/treegoram/mainpage.do">Treegoram</a>
					</div>
					<div id="top-panel" class="col-xs-12 col-sm-10">
						<div class="row">
							<div class="col-xs-8 col-sm-4">
								<div id="search">
									<input type="text" placeholder="search" /> <i
										class="fa fa-search"></i>
								</div>
							</div>
							<div class="col-xs-4 col-sm-8 top-panel-right">
								<a href="/treegoram/productAddPage.do">
									<button type="button" class="btn btn-outline-success"
										style="margin-top: 5px;">Create Product</button>
								</a>

								<ul class="nav navbar-nav pull-right panel-menu">
									<li class="hidden-xs"><a href="index.html"
										class="modal-link"> <i class="fa fa-bell"></i> <span
											class="badge">7</span>
									</a></li>
									<li class="hidden-xs"><a class="ajax-link"
										href="ajax/calendar.html"> <i class="fa fa-calendar"></i>
											<span class="badge">7</span>
									</a></li>
									<li class="hidden-xs"><a href="ajax/page_messages.html"
										class="ajax-link"> <i class="fa fa-envelope"></i> <span
											class="badge">7</span>
									</a></li>
									<li class="dropdown"><a href="#"
										class="dropdown-toggle account" data-toggle="dropdown">
											<div class="avatar">
												<img src="img/avatar.jpg" class="img-circle" alt="avatar" />
											</div> <i class="fa fa-angle-down pull-right"></i>
											<div class="user-mini pull-right">
												<span class="welcome">Welcome,</span> <span>Jane
													Devoops</span>
											</div>
									</a>
										<ul class="dropdown-menu">
											<li><a href="#"> <i class="fa fa-user"></i> <span>Profile</span>
											</a></li>
											<li><a href="ajax/page_messages.html" class="ajax-link">
													<i class="fa fa-envelope"></i> <span>Messages</span>
											</a></li>
											<li><a href="ajax/gallery_simple.html" class="ajax-link">
													<i class="fa fa-picture-o"></i> <span>Albums</span>
											</a></li>
											<li><a href="ajax/calendar.html" class="ajax-link">
													<i class="fa fa-tasks"></i> <span>Tasks</span>
											</a></li>
											<li><a href="#"> <i class="fa fa-cog"></i> <span>Settings</span>
											</a></li>
											<li><a href="#"> <i class="fa fa-power-off"></i> <span>Logout</span>
											</a></li>
										</ul></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
