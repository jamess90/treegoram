<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<script src="plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">

	function datgleClick(num){
		
        var state = $('#hideshow'+num).css('display'); // state 변수에 ID가 moreMenu인 요소의 display의 속성을 '대입'
        if(state == 'none'){ // state가 none 상태일경우
        	$('#hideshow'+num).show(); // ID가 moreMenu인 요소를 show();
        	
        	param ="timelineNo="+num;
        	
    		$.ajax({
    			"url":"/treegoram/commentShow.do",
    			"type":"POST",
    			"data":param,   // id=$("#id").val()
    			"dataType":"json",
    			"success":function(obj){
    				var txt="";
    				$.each(obj,function(){
    					txt = txt+"<tr><td>"+this.userName+"</td><td>"+this.timelineContent+"</td><td>"+this.timelineTime+"</td><br>";
    				})
    				$('.commentLine[data_no='+num+']').html(txt);
    			},
    			"error":function(error,status,xhr){
    				alert('에러발생');
    			},
    		});
        	
        }else{ // 그 외에는
        	$('#hideshow'+num).hide(); // ID가 moreMenu인 요소를 hide();        
        }
	}


	function datgle(number){
		var datgle = $('.datgle[data_no='+number+']').val();
		var userNo=$('.user_no[data_no='+number+']').val();
		var timelineNo = number;
		
		var param = "datgle="+datgle+"&userNo="+userNo+"&timelineNo="+number;
		alert(param);
		
		$.ajax({
			"url":"/treegoram/insertTimelineComment.do",
			"type":"POST",
			"data":param,   // id=$("#id").val()
			"dataType":"json",
			"success":function(obj){
				var txt="";
				$.each(obj,function(){
					txt = txt+"<tr><td>"+this.userName+"</td><td>"+this.timelineContent+"</td><td>"+this.timelineTime+"</td><br>";
				})
				$('.commentLine[data_no='+number+']').html(txt);
			},
			"error":function(error,status,xhr){
				alert(xhr.status);
			},
			"beforeSend":function(){
 				if(! $(".datgle").val()){
					alert("입력할 댓글을 넣으세요");
					return false;
				} 
			}
		});
	}	

</script>


<style type="text/css">
td {
    padding: 20px;
    text-align: left;
    border-bottom: 2px solid #ddd;
    height: 35px;
}


body {
	margin-top: 20px;
	background-color: #F5F7FA;
}
/*
    these are just the default styles used in the Cycle2 demo pages.  
    you can use these styles or any others that you wish.
*/


/* set border-box so that percents can be used for width, padding, etc (personal preference) */
.cycle-slideshow, .cycle-slideshow * { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; }


.cycle-slideshow { width: 60%; min-width: 200px; max-width: 500px; margin: 10px auto; padding: 0; position: relative;
    background: url(http://malsup.github.com/images/spinner.gif) 50% 50% no-repeat;

 }

/* slideshow images (for most of the demos, these are the actual "slides") */
.cycle-slideshow img { 
    /* 
    some of these styles will be set by the plugin (by default) but setting them here
    helps avoid flash-of-unstyled-content
    */
    position: absolute; top: 0; left: 0;
    width: 100%; padding: 0; display: block;
}

/* in case script does not load */
.cycle-slideshow img:first-child {
    position: static; z-index: 100;
}

/* pager */
.cycle-pager { 
    text-align: center; width: 100%; z-index: 500; position: absolute; top: 10px; overflow: hidden;
}
.cycle-pager span { 
    font-family: arial; font-size: 50px; width: 16px; height: 16px; 
    display: inline-block; color: #ddd; cursor: pointer; 
}
.cycle-pager span.cycle-pager-active { color: #D69746;}
.cycle-pager > * { cursor: pointer;}


/* caption */
.cycle-caption { position: absolute; color: white; bottom: 15px; right: 15px; z-index: 700; }


/* overlay */
.cycle-overlay { 
    font-family: tahoma, arial;
    position: absolute; bottom: 0; width: 100%; z-index: 600;
    background: black; color: white; padding: 15px; opacity: .5;
}


/* prev / next links */
.cycle-prev, .cycle-next { position: absolute; top: 0; width: 30%; opacity: 0; filter: alpha(opacity=0); z-index: 800; height: 100%; cursor: pointer; }
.cycle-prev { left: 0;  background: url(http://malsup.github.com/images/left.png) 50% 50% no-repeat;}
.cycle-next { right: 0; background: url(http://malsup.github.com/images/right.png) 50% 50% no-repeat;}
.cycle-prev:hover, .cycle-next:hover { opacity: .7; filter: alpha(opacity=70) }

.disabled { opacity: .5; filter:alpha(opacity=50); }


/* display paused text on top of paused slideshow */
.cycle-paused:after {
    content: 'Paused'; color: white; background: black; padding: 10px;
    z-index: 500; position: absolute; top: 10px; right: 10px;
    border-radius: 10px;
    opacity: .5; filter: alpha(opacity=50);
}

/* 
    media queries 
    some style overrides to make things more pleasant on mobile devices
*/

@media only screen and (max-width: 480px), only screen and (max-device-width: 480px) {
    .cycle-slideshow { width: 200px;}
    .cycle-overlay { padding: 4px }
    .cycle-caption { bottom: 4px; right: 4px }
}

.timeline-panel.panel.fade.in.panel-default.panel-fill {
	margin-top: 50px;
    margin-left: 100px;
    margin-bottom: 10px;
    margin-right: 250px;
}

.bg-grd-red {
	background: linear-gradient(45deg, #cc0576 0, rgba(204, 5, 118, 0) 70%),
		linear-gradient(135deg, #cf022b 10%, rgba(207, 2, 43, 0) 80%),
		linear-gradient(225deg, #e91c01 10%, rgba(233, 28, 1, 0) 80%),
		linear-gradient(315deg, #ca7f07 100%, rgba(202, 127, 7, 0) 70%)
		!important;
}

.content-hero:first-child {
	margin-top: -15px;
}

.content-hero {
	position: relative;
	padding: 15px;
	margin-right: -15px;
	margin-left: -15px;
	margin-bottom: 30px;
	background-color: #fff;
	min-height: 150px;
	overflow: hidden;
}

.content-hero.content-hero-sm {
	min-height: 80px;
}

.content-hero+.content-hero {
	margin-top: -30px;
}

.content-hero .content-hero-embed {
	position: absolute;
	top: 0;
	right: 0;
	width: 100%;
	z-index: 1;
}

.content-hero .content-hero-overlay {
	position: absolute;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	z-index: 2;
	background-color: #F5F7FA;
	opacity: .6;
	filter: alpha(opacity = 60);
}

.bg-grd-red {
	background: linear-gradient(45deg, #cc0576 0, rgba(204, 5, 118, 0) 70%),
		linear-gradient(135deg, #cf022b 10%, rgba(207, 2, 43, 0) 80%),
		linear-gradient(225deg, #e91c01 10%, rgba(233, 28, 1, 0) 80%),
		linear-gradient(315deg, #ca7f07 100%, rgba(202, 127, 7, 0) 70%)
		!important;
}

.content-hero .content-hero-body {
	position: relative;
	z-index: 3;
}

.btn-icon.btn-default {
	color: rgba(22, 24, 27, .87);
	background-color: transparent;
}

.kit-avatar.kit-avatar-64 {
	border: 2px solid rgba(22, 24, 27, .12);
	border-radius: 64px;
}

a.kit-avatar {
	color: inherit;
	text-decoration: none;
}

.no-padding {
	padding: 0;
}

.border-white {
	border-color: #fff !important;
}

.kit-avatar {
	position: relative;
	display: inline-block;
	padding: 2px;
	text-align: center;
	transition: border-color ease .3s;
}

.kit-avatar.kit-avatar-64>img {
	width: 64px;
	height: auto;
	border-radius: 32px;
}

.kit-avatar.kit-avatar-42 {
	border: 2px solid rgba(22, 24, 27, .12);
	border-radius: 42px;
}

.kit-avatar.kit-avatar-42>img {
	width: 42px;
	height: auto;
	border-radius: 21px;
}

.text-red {
	color: #ED5565 !important;
}

.h2, .headline, h2 {
	font-size: 24px;
	font-weight: 400;
	line-height: 32px;
}

.timeline, .timeline>li {
	list-style: none;
}

.timeline {
	position: relative;
	display: block;
	margin: 0;
	padding: 10px 0;
}

.timeline:after, .timeline:before {
	content: " ";
	display: table;
}

.timeline>li:nth-child(even) {
	float: left;
	clear: left;
}

.timeline, .timeline>li {
	list-style: none;
}

.timeline-group {
	display: block;
	position: relative;
	margin: 20px 0;
	text-align: center;
	float: none !important;
	z-index: 1;
}

.btn {
	font-weight: 500;
	line-height: 20px;
}

.btn-default {
	color: rgba(22, 24, 27, .87);
	background-color: #E6E9ED;
	border-color: transparent;
}

.timeline>li:nth-child(odd) {
	float: right;
	clear: right;
}

.timeline, .timeline>li {
	list-style: none;
}

.timeline-line {
	display: inline-block;
	position: absolute;
	top: 0;
	bottom: 0;
	left: 50%;
	width: 4px;
	background-color: rgba(22, 24, 27, .12);
	-webkit-transform: translate(-50%, 0);
	-ms-transform: translate(-50%, 0);
	transform: translate(-50%, 0);
}

.timeline>li:nth-child(even) {
	float: left;
	clear: left;
}

.timeline-line+.timeline-item {
	margin-top: -20px;
}

.timeline, .timeline>li {
	list-style: none;
}

.timeline-item {
	position: relative;
	display: inline-block;
	width: 50%;
	padding: 0 30px 20px;
}

.timeline-poster {
	margin-top: -20px;
}

.timeline-item:nth-child(even)>.timeline-badge {
	right: -6px;
}

.timeline-item>.timeline-badge {
	position: absolute;
	top: 12px;
	z-index: 1;
}

.timeline-item>.timeline-badge>a {
	display: inline-block;
	width: 12px;
	height: 12px;
	border: 2px solid #5D9CEC;
	border-radius: 24px;
	background-color: #fff;
	text-decoration: none;
	transition: all ease .3s;
}

.border-teal {
	border-color: #48CFAD !important;
}

.panel-default.panel-fill, .panel-default.panel-fill>.panel-heading {
	background-color: #fff;
}

.fade.in {
	opacity: 1;
}

.panel {
	border: none;
	box-shadow: 0 1px 3px -1px rgba(22, 24, 27, .26);
	transition: background-color 250ms ease, opacity 250ms linear;
}

.panel-default {
	border-color: rgba(22, 24, 27, .12);
}

.panel {
	margin-bottom: 20px;
	background-color: #fff;
	border-radius: 2px;
}

.panel-body {
	padding: 12px;
}

.nav-tabs {
	border-bottom: none;
}

.nav {
	margin-bottom: 0;
	padding-left: 0;
	list-style: none;
}

.nav-tabs>li {
	margin-bottom: 0;
}

.nav-tabs>li {
	float: left;
}

.nav>li {
	position: relative;
	display: block;
}

.nav.nav-contrast-red.nav-tabs>li>a {
	color: inherit;
}

.nav-tabs>li>a {
	margin-right: 0;
	border: none;
	border-bottom: 2px solid transparent;
	border-radius: 0;
	color: rgba(22, 24, 27, .87);
}

.nav-tabs>li>a {
	line-height: 1.42857;
}

.nav>li>a {
	position: relative;
	display: block;
	padding: 12px;
}

.nav-tabs a {
	outline: 0;
}

.nav.nav-contrast-red.nav-tabs>li.active>a, .nav.nav-contrast-red.nav-tabs>li.active>a:focus,
	.nav.nav-contrast-red.nav-tabs>li.active>a:hover, .nav.nav-contrast-red.nav-tabs>li>a:focus,
	.nav.nav-contrast-red.nav-tabs>li>a:hover {
	color: #ED5565;
	border-bottom: 2px solid #ED5565;
}

.nav.nav-contrast-red.nav-tabs>li>a {
	color: inherit;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover,
	.nav-tabs>li.open>a, .nav-tabs>li.open>a:focus, .nav-tabs>li.open>a:hover
	{
	border: none;
	border-bottom: 2px solid #5D9CEC;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover
	{
	color: #5D9CEC;
	background-color: transparent;
	cursor: default;
}

.nav-tabs>li>a {
	margin-right: 0;
	border: none;
	border-bottom: 2px solid transparent;
	border-radius: 0;
	color: rgba(22, 24, 27, .87);
}

.nav-tabs>li>a {
	line-height: 1.42857;
}

.nav>li>a {
	position: relative;
	display: block;
	padding: 12px;
}

.nav-tabs a {
	outline: 0;
}

.pt-1x {
	padding-top: 6px;
}

.no-border {
	border: none !important;
}

.no-resize {
	resize: none;
}

.form-control, .form-control:focus {
	box-shadow: none;
}

.form-control {
	display: block;
	width: 100%;
	height: 34px;
	padding: 6px 12px;
	font-size: 14px;
	line-height: 1.42857;
	color: rgba(22, 24, 27, .87);
	background-color: #fff;
	background-image: none;
	border: 1px solid rgba(22, 24, 27, .12);
	border-radius: 2px;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

.panel-footer {
	background-color: transparent;
}

.panel-footer {
	padding: 6px 12px;
	border-top: 1px solid rgba(22, 24, 27, .12);
	border-bottom-right-radius: 1px;
	border-bottom-left-radius: 1px;
}

.btn-group, .btn-group-vertical {
	position: relative;
	display: inline-block;
	vertical-align: middle;
}

.btn-group>.btn:first-child:not (:last-child ):not (.dropdown-toggle ) {
	border-bottom-right-radius: 0;
	border-top-right-radius: 0;
}

.btn-group>.btn:first-child {
	margin-left: 0;
}

.timeline-poster .btn-link {
	color: rgba(22, 24, 27, .54);
}

.btn-group-vertical>.btn, .btn-group>.btn {
	position: relative;
	float: left;
}

.btn {
	font-weight: 500;
	line-height: 20px;
}

.btn-link, .btn-link:active, .btn-link:focus, .btn-link:hover {
	border-color: transparent;
}

.btn-link, .btn-link.active, .btn-link:active, .btn-link[disabled],
	fieldset[disabled] .btn-link {
	background-color: transparent;
	box-shadow: none;
}

.btn-link {
	color: #5D9CEC;
	font-weight: 400;
	border-radius: 0;
}

.timeline-item:nth-child(even):after {
	right: 20px;
	top: 11px;
	border-top: 11px solid transparent;
	border-bottom: 11px solid transparent;
	border-left: 11px solid #fff;
}

.timeline-item:nth-child(even):after, .timeline-item:nth-child(even):before
	{
	content: '';
	position: absolute;
	right: 19px;
	top: 10px;
	width: 0;
	height: 0;
	border-top: 12px solid transparent;
	border-bottom: 12px solid transparent;
	border-left: 12px solid rgba(22, 24, 27, .12);
	z-index: 1;
}

.timeline-item:nth-child(odd)>.timeline-badge {
	left: -6px;
}

.timeline-item>.timeline-badge {
	position: absolute;
	top: 12px;
	z-index: 1;
}

.border-orange {
	border-color: #FC6E51 !important;
}

.timeline-item>.timeline-panel>.timeline-resume {
	padding-top: 0;
	border: none;
}

.panel-body+.panel-body {
	border-top: 1px solid rgba(22, 24, 27, .12);
}

.panel-body {
	padding: 12px;
}

.media:first-child {
	margin-top: 0;
}

.mb-2x {
	margin-bottom: 12px;
}

.media, .media-body {
	overflow: initial;
}

.media, .media-body {
	zoom: 1;
}

.kit-avatar.kit-avatar-36 {
	border: 1px solid rgba(22, 24, 27, .12);
	border-radius: 36px;
}

.kit-avatar.kit-avatar-36>img {
	width: 36px;
	height: auto;
	border-radius: 18px;
}

p {
	margin: 0 0 10px;
}

.timeline-item>.timeline-panel>.timeline-resume {
	padding-top: 0;
	border: none;
}

.panel-body+.panel-body {
	border-top: 1px solid rgba(22, 24, 27, .12);
}

.kit-avatar.kit-avatar-24 {
	border: 1px solid rgba(22, 24, 27, .12);
	border-radius: 24px;
}

.no-border {
	border: none !important;
}

.align-middle {
	vertical-align: middle;
}

.kit-avatar.kit-avatar-24>img {
	width: 24px;
	height: auto;
	border-radius: 12px;
}

.bordered, .bordered-bottom {
	border-bottom: 1px solid rgba(22, 24, 27, .12) !important;
}

.kit-avatar.kit-avatar-28 {
	border: 1px solid rgba(22, 24, 27, .12);
	border-radius: 28px;
}

.kit-avatar.kit-avatar-28>img {
	width: 28px;
	height: auto;
	border-radius: 14px;
}

.input-group.input-group-in {
	border: 1px solid rgba(22, 24, 27, .12);
	border-radius: 2px;
	background-color: #fff;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}

.input-group.input-group-in>.input-group-btn:last-child {
	padding-right: 0;
	padding-left: 6px;
}

.input-group-btn {
	position: relative;
	font-size: 0;
	white-space: nowrap;
}

.input-group-addon, .input-group-btn {
	width: 1%;
	white-space: nowrap;
	vertical-align: middle;
}

.input-group .form-control, .input-group-addon, .input-group-btn {
	display: table-cell;
}

.btn-bordered.btn-default {
	color: rgba(22, 24, 27, .87);
	border-color: rgba(22, 24, 27, .12);
}

.btn-bordered {
	background-color: transparent;
	border: 1px solid transparent;
}

.btn {
	font-weight: 500;
	line-height: 20px;
}

.btn-circle.btn-xs {
	width: 30px;
	height: 30px;
	line-height: 28px;
}

.btn-circle {
	padding: 0;
	width: 42px;
	height: 42px;
	line-height: 40px;
	border-radius: 21px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}

.input-group.input-group-in>.input-group-btn:last-child>.btn {
	border-right: none;
	border-left: 1px solid rgba(22, 24, 27, .12);
}

.input-group.input-group-in>.input-group-btn>.btn {
	color: inherit;
	padding: 0 12px;
	border: none;
	background-color: transparent;
	outline: 0;
	box-shadow: none;
	height: auto;
}

.timeline-item:nth-child(odd):after {
	left: 20px;
	top: 11px;
	border-top: 11px solid transparent;
	border-bottom: 11px solid transparent;
	border-right: 11px solid #fff;
}

.timeline-item:nth-child(odd):after, .timeline-item:nth-child(odd):before
	{
	content: '';
	position: absolute;
	left: 19px;
	top: 10px;
	width: 0;
	height: 0;
	border-top: 12px solid transparent;
	border-bottom: 12px solid transparent;
	border-right: 12px solid rgba(22, 24, 27, .12);
	z-index: 1;
}

nav {
	margin-top: 3%;
	line-height: 50px;
	background-color: #5D5D5D;
	padding: 15px;
	min-height: 2000px;
	width: 200px;
	float: left;
}

.bod {
	margin-top: 3%;
	padding: 15px;
	min-height: 2100px;
	float: left;
	width: 67%;
	background: white;
}

.chat {
	padding: 15px;
	min-height: 2100px;
	float: right;
	width: 19.8%;
	background: #4C4C4C;
}

footer {
	background-color: #5D5D5D;
	padding-top: 20px;
	text-align: center;
	height: 50px;
	clear: both;
	color: white;
}
</style>
<meta charset="utf-8">
<title>TreeGoram</title>
<script type="text/javascript" src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<meta name="description" content="description">
<meta name="author" content="DevOOPS">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="plugins/bootstrap/bootstrap.css" rel="stylesheet">
<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Righteous'
	rel='stylesheet' type='text/css'>
<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
<link href="plugins/xcharts/xcharts.min.css" rel="stylesheet">
<link href="plugins/select2/select2.css" rel="stylesheet">
<link href="plugins/justified-gallery/justifiedGallery.css"
	rel="stylesheet">
<link href="css/style_v2.css" rel="stylesheet">
<link href="plugins/chartist/chartist.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->

</head>
<body>


	<!--Start Header-->
	<%@ include file="../main/main_header.jsp"%>
	<!--End Header-->

	<nav>
		<%@ include file="../main/main_sidebar.jsp"%>
	</nav>

	<section class="bod">
		<form action="/treegoram/updateTimeline.do" method="POST" enctype="multipart/form-data">
			<input type="hidden" value="${requestScope.timelineList.timelineNo}" name="timelineNo"> 
			<div class="modal-dialog modal-md">
			<div class="modal-content">
      			<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">타임라인 수정</h4>
      		</div>
      		

			<div class="modal-body">
				<div class="form-group">
					
					<div class="inputGroupContainer">
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> 
								<input name="timelineTitle" placeholder="${requestScope.timelineList.timelineTitle }"
								type="text" value="${requestScope.timelineList.timelineTitle }">
						</div>
					</div>
				</div>
			
					
				<div class="input-group">
					<span class="input-group-addon">
					<i class="glyphicon glyphicon-pencil"></i></span>
						<textarea rows="10" cols="50" name="timelineContent">${requestScope.timelineList.timelineContent }</textarea>
				</div>					
					<br>
					<div>
					현재파일 :
							<c:forEach items="${requestScope.timelineList.photoList}" var="plist">
								${plist.timelinePhotoFile}, 
							</c:forEach>
							<br>
					수정할 파일:<br>
						<input name="upfiles" class="form-control" type="file" >
						<input name="upfiles" class="form-control" type="file">	
						</div>
					
				<br>
				
				<div align="center">
				<button type="submit" class="btn btn-success" >수정하기</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
			</div>
			</div>
</form>

	</section>

	<footer>
		<%@ include file="../main/main_footer.jsp"%>
	</footer>

	<!--End Container-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<!-- <script src="http://code.jquery.com/jquery.js"></script> -->
	
	<script src="http://malsup.github.com/jquery.cycle2.js"></script>
	<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed
 -->
	<script src="plugins/bootstrap/bootstrap.min.js"></script>
	<script src="plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
	<script src="plugins/tinymce/tinymce.min.js"></script>
	<script src="plugins/tinymce/jquery.tinymce.min.js"></script>

	<!-- All functions for this theme + document.ready processing -->
	<script src="js/devoops.js"></script>
</body>
</html>
