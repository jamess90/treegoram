<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<a href="${initParam.rootPath}/main.do">메인페이지</a>
<h2>업로드된 사진</h2>
사진설명 : ${requestScope.comment }<br>
사진 이름 : ${requestScope.imageName }<br>
사진 크기 : ${requestScope.imageSize }<br>
<img src="/spring_mvc_05_fileupload/upImage/${requestScope.imageName }">

<hr>
<h2>등록한 게시물</h2>
글제목 : ${requestScope.title } <br>
글내용 : ${requestScope.content }<br>
업로드된 파일<br>
<c:forEach items="${requestScope.fileNames }" var="fileName">
<a href="/spring_mvc_05_fileupload/uploadFile/${fileName }">
${fileName }
</a>
<br>
</c:forEach>


<h3>DownloadView를 이용해 다운로드 처리</h3>
<c:forEach items="${requestScope.fileNames }" var="fileName">
	<a href="/spring_mvc_05_fileupload/download.do?fileName=${fileName }">
		${fileName }
	</a>
</c:forEach>
</body>
</html>
















