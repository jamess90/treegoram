<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<title>TreeGoram</title>
		<script type="text/javascript" src="plugins/jquery/jquery.min.js"></script>
		<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
		<meta name="description" content="description">
		<meta name="author" content="DevOOPS">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="plugins/bootstrap/bootstrap.css" rel="stylesheet">
		<link href="plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
		<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
		<link href="plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
		<link href="plugins/xcharts/xcharts.min.css" rel="stylesheet">
		<link href="plugins/select2/select2.css" rel="stylesheet">
		<link href="plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
		<link href="css/style_v2.css" rel="stylesheet">
		<link href="plugins/chartist/chartist.min.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	
	<script type="text/javascript">
	$( function() {
	    $( "#endDate" ).datepicker({
	    minDate: 0,
	    maxDate: 2,
		 dateFormat:"yy-mm-dd 23:59:59"
	    });
	});
	
	function checkfield(){
		 
		 if(document.addjoin.productName.value==""){ //id값이 없을 경우
		 alert("상품명을 입력하세요");         //메세지 경고창을 띄운 후
		 document.addjoin.productName.focus();     // id 텍스트박스에 커서를 위치
		 return false;
		 
		 }
		 
		 if(document.addjoin.startPrice.value==""){
		 alert("시작가를 입력하세요");
		 document.addjoin.startPrice.focus();
		 return false;
		 
		 }
		 
		 if(document.addjoin.promptPrice.value==""){
		 alert("즉시구매가를 입력하세요");
		 document.addjoin.promptPrice.focus();
		 return false;
		 
		 }
		 
		 if(document.addjoin.endDate.value==""){
		 alert("경매마감날짜를 선택하세요");
		 document.addjoin.endDate.focus();
		 return false;
		 
		 }
		 
		 if(document.addjoin.mycontent.value==""){
		 alert("상품설명을 입력하세요");
		 document.addjoin.mycontent.focus();
		 return false;	 
		 }
		 
		 
		 if(document.addjoin.startPrice.value > document.addjoin.promptPrice.value){
		 alert("시작가가 즉시구매가 보다 클 수 없습니다.");
		 document.addjoin.startPrice.focus();
		 return false;
		 }		 
		 
		}
	</script>
	
	</head>
<body>


<!--Start Header-->
<%@ include file="../main/main_header.jsp" %>
<!--End Header-->

<%@ include file="../main/main_sidebar.jsp" %>

<section class="bod">
<br><br><br>

<form name="addjoin" class="form-horizontal" action="/treegoram/productAdd.do" method="post" id="reg_form" enctype="multipart/form-data" onsubmit="return checkfield()">
    <fieldset>
      <input type="hidden" value="${sessionScope.userNo}" name="userNo">
   
      <legend><b> &nbsp;&nbsp;&nbsp;Product Register </b></legend>
    
 	
      <br>
      <div class="form-group">
        <label class="col-md-4 control-label">Product Name</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input  name="productName" placeholder="Product Name" class="form-control"  type="text" id="productName">
          </div>
        </div>
      </div>
      
      
      <div class="form-group">
        <label class="col-md-4 control-label" >Start Price</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input name="startPrice" placeholder="Product Price" class="form-control"  type="number" id="startPrice">
          </div>
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-md-4 control-label" >즉시 구매가</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input name="PromptPrice" placeholder="PromptPrice" class="form-control"  type="number" id="promptPrice">
          </div>
        </div>
      </div>
      
      <div class="form-group">
        <label class="col-md-4 control-label">Product Category</label>
        <div class="col-md-6 selectContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <select name="category" class="form-control selectpicker" >
			<option value="wear">의류(Wear)</option>
			<option value="funiture">가구(Funiture)</option>
			<option value="life">생활(Life)</option>
			<option value="electronics">전자제품(electronics)</option>
			<option value="sports">스포츠(sports)</option>
			<option value="etc">기타(etc)</option>
            </select>
          </div>
        </div>
      </div>
      
      
       <div class="form-group">
        <label class="col-md-4 control-label">경매단위</label>
        <div class="col-md-6 selectContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
            <select name="raiseUnit" class="form-control selectpicker" >
			<option value=100>100원</option>
			<option value=1000>1000원</option>
			<option value=10000>10000원</option>
			</select>
          </div>
        </div>
      </div>
      
      
     <div class="form-group">
        <label class="col-md-4 control-label">마감 날짜</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
            <input  name="endDate" placeholder="endDate" class="form-control"  type="text" id="endDate">
          </div>
        </div>
      </div>
      
      
    
      <div class="form-group">
        <label class="col-md-4 control-label">Content </label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
            <textarea id="mycontent" rows="10"  cols="50" class="form-control" name="text" placeholder="About"></textarea>
          </div>
        </div>
      </div>
      
      
      <div class="form-group">
        <label class="col-md-4 control-label">Register File1</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon"></i></span>
            <input  name="upfiles" placeholder="Product Name" class="form-control"  type="file">
          </div>
        </div>
      </div>
      
     <div class="form-group">
        <label class="col-md-4 control-label">Register File2</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon"></i></span>
            <input name="upfiles" placeholder="Product Name" class="form-control"  type="file">
          </div>
        </div>
      </div>
      
     <div class="form-group">
        <label class="col-md-4 control-label">Register File3</label>
        <div class="col-md-6  inputGroupContainer">
          <div class="input-group"> <span class="input-group-addon"><i class="glyphicon"></i></span>
            <input  name="upfiles" placeholder="Product Name" class="form-control"  type="file">
          </div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label"></label>
        <div class="col-md-4">
          <button type="submit" class="btn btn-warning">Register <span class="glyphicon glyphicon-send"></span></button>
        </div>
      </div>
    </fieldset>
    
  </form>  
</section>

<%@ include file="../main/main_footer.jsp" %>

<!--End Container-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <script src="http://code.jquery.com/jquery.js"></script> -->
<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed
 -->
 <script src="plugins/bootstrap/bootstrap.min.js"></script>
<script src="plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
<script src="plugins/tinymce/tinymce.min.js"></script>
<script src="plugins/tinymce/jquery.tinymce.min.js"></script>

<!-- All functions for this theme + document.ready processing -->
<script src="js/devoops.js"></script>
</body>
</html>
