<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				console.log(e.target.result);
				document.getElementById("imgDiv").innerHTML =
					"<img id='blah' src='"+e.target.result+"' alt='이미지' width='150px' height='150px'>";
				}
			reader.readAsDataURL(input.files[0]);
		}
	}
</script>
<style>
.form-control2 {
	/* display: block; */
	float: right;
	width: 75%;
	height: 34px;
	padding: 6px 12px;
	font-size: 14px;
	line-height: 1.42857143;
	color: #555;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	-webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow
		ease-in-out .15s;
	-o-transition: border-color ease-in-out .15s, box-shadow ease-in-out
		.15s;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
</style>
<div style="margin-top: 30px; margin-left: 50px;">
	<h1>
		<b>회원가입 </b>
	</h1><br>
	<form action="/treegoram/join" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="email">이메일 </label>
			<input type="text" name="email" class="form-control2" id="email" required>
		</div>
		<div class="form-group">
			<label for="password">비밀번호 </label>
			<input type="password" name="password" class="form-control2" id="password" required>
		</div>
		<div class="form-group">
			<label for="userName">이름 </label>
			<input type="text" name="userName" class="form-control2" id="userName" required>
		</div>
		<div class="form-group">
			<label for="phone">전화번호 </label>
			<input type="text" id="phone3" size="4" name="phone3" class="form-control2" style="width: 23%; margin-left: 10px;">
			<input type="text" id="phone2" size="4" name="phone2" class="form-control2" style="width: 23%; margin-left: 10px;">
			<select id="phone1" name="phone1" class="form-control2" style="width: 23%; margin-left: 10px;">
				<option>010</option>
				<option>011</option>
				<option>016</option>
				<option>017</option>
				<option>018</option>
				<option>019</option>
			</select>
		</div>
		<div class="form-group">
			<label for="userPostNumber">우편번호 </label>
			<input type="text" name="userPostNumber" class="form-control2" id="userPostNumber" style="width:150px; " required>
			<input type="button" value="우편번호" class="btn btn-default" onclick="openDaumPostcode()" style="margin-left:33px;"/>
		</div>
		<div id="layer" style="display: none; border: 5px solid; position: fixed; width: 500px; height: 500px; left: 50%; margin-left: -250px; top: 50%; margin-top: -250px; overflow: hidden">
			<img src="http://i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer"
				style="cursor: pointer; position: absolute; right: -3px; top: -3px" onclick="closeDaumPostcode()">
		</div>
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		<script>
			var element = document.getElementById('layer');

			function closeDaumPostcode() {
				element.style.display = 'none';
			}

			function openDaumPostcode() {
				new daum.Postcode(
						{
							oncomplete : function(data) {
								document.getElementById("userPostNumber").value = data.zonecode;
								document.getElementById("userAddress1").value = data.address;
								document.getElementById("userAddress2").focus();
								element.style.display = 'none';
							},
							width : '100%',
							height : '100%'
						}).embed(element);

				element.style.display = 'block';
			}
		</script>
		<div class="form-group">
			<label for="userAddress1">주소 </label>
			<input type="text" name="userAddress1" class="form-control2" id="userAddress1" required>
		</div>
		<div class="form-group">
			<label for="userAddress2">상세주소 </label>
			<input type="text" name="userAddress2" class="form-control2" id="userAddress2" required>
		</div>
		<div class="form-group">
			<label for="birthday">생년월일 </label>
			<input type="date" name="birthday" class="form-control2" id="birthday" required>
		</div>
		<div class="form-group">
			<label for="userPhotoFile">프로필사진 </label>
			<input type="file" name="upfiles" class="form-control2" id="userPhoto" onchange="readURL(this);">
				<div id="imgDiv" style="margin-left: 100px"></div>
		</div>
		<button type="submit" class="btn btn-default" style="margin-left: 150px">가입</button>
	</form>
</div>