<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/flipclock.css">	<!--  남은시간 countdown용 -->
	<script src="js/flipclock.js"></script>				<!--  남은시간 countdown용 -->

<script type="text/javascript">
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				console.log(e.target.result);
				document.getElementById("imgDiv").innerHTML =
					"<img id='blah' src='"+e.target.result+"' alt='이미지' width='150px' height='150px'>";
				}
			reader.readAsDataURL(input.files[0]);
		}
	}

</script>
<style>
.form-control2 {
	/* display: block; */
	float: right;
	width: 75%;
	height: 34px;
	padding: 6px 12px;
	font-size: 14px;
	line-height: 1.42857143;
	color: #555;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	-webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow
		ease-in-out .15s;
	-o-transition: border-color ease-in-out .15s, box-shadow ease-in-out
		.15s;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
</style>
<div style="margin-top: 30px; margin-left: 50px; " class="col-xs-9">
	<h1>
		<b>회원정보</b>
	</h1><br>
	<form action="/treegoram/join" method="post" enctype="multipart/form-data">
		<div class="form-group">
			<label for="email">이메일 </label>
			<input type="text" name="email" class="form-control2" id="email" value="${userInfo.email}" required>
		</div>
		<div class="form-group">
			<label for="password">비밀번호 </label>
			<input type="password" name="password" class="form-control2" id="password" value="${userInfo.password}" required>
		</div>
		<div class="form-group">
			<label for="userName">이름 </label>
			<input type="text" name="userName" class="form-control2" id="userName" value="${userInfo.userName}" required>
		</div>
		<div class="form-group">
			<label for="phone">전화번호</label>
			<input type="text" name="phone" class="form-control2" id="phone" value="0${userInfo.phone}" required>
		</div>
		<div class="form-group">
			<label for="userPostNumber">우편번호 </label>
			<input type="text" name="userPostNumber" class="form-control2" id="userPostNumber" style="width:150px; " value="${userInfo.userPostNumber}" required>
			<input type="button" value="우편번호" class="btn btn-default" onclick="openDaumPostcode()" style="margin-left:500px;"/>
		</div>
		<div id="layer" style="display: none; border: 5px solid; position: fixed; width: 500px; height: 500px; left: 50%; margin-left: -250px; top: 50%; margin-top: -250px; overflow: hidden">
			<img src="http://i1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnCloseLayer"
				style="cursor: pointer; position: absolute; right: -3px; top: -3px" onclick="closeDaumPostcode()">
		</div>
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		<script>
			var element = document.getElementById('layer');

			function closeDaumPostcode() {
				element.style.display = 'none';
			}

			function openDaumPostcode() {
				new daum.Postcode(
						{
							oncomplete : function(data) {
								document.getElementById("userPostNumber").value = data.zonecode;
								document.getElementById("userAddress1").value = data.address;
								document.getElementById("userAddress2").focus();
								element.style.display = 'none';
							},
							width : '100%',
							height : '100%'
						}).embed(element);

				element.style.display = 'block';
			}
		</script>
		<div class="form-group">
			<label for="userAddress1">주소 </label>
			<input type="text" name="userAddress1" class="form-control2" id="userAddress1" value="${userInfo.userAddress1}" required>
		</div>
		<div class="form-group">
			<label for="userAddress2">상세주소 </label>
			<input type="text" name="userAddress2" class="form-control2" id="userAddress2" value="${userInfo.userAddress2}" required>
		</div>
		<div class="form-group">
			<label for="birthday">생년월일 </label>
			<input type="date" name="birthday" class="form-control2" id="birthday" value="${userInfo.birthday}" required>
		</div>
	<!-- 	<div class="form-group">
			<label for="userPhotoFile">프로필사진 </label>
			<input type="file" name="upfiles" class="form-control2" id="upfiles" onchange="readURL(this);">
				<div id="imgDiv" style="margin-left: 100px"></div>
		</div> -->
		<button type="submit" class="btn btn-default" formaction="/treegoram/editInfo.do">수정</button>
		<button type="submit" class="btn btn-default" formaction="/treegoram/secession.do">탈퇴</button>
	</form>
</div>