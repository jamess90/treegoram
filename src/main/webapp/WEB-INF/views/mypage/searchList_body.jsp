<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/flipclock.css">	<!--  남은시간 countdown용 -->
	<script src="js/flipclock.js"></script>				<!--  남은시간 countdown용 -->
	
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-xs-8 col-sm-4">
				<div id="search">
					<input id="searchWord" type="text" placeholder="이메일  또는 이름으로 친구검색 " value="${fdto.searchWord}" onkeydown="keydown()" autofocus required/> <i class="fa fa-search"></i>
					<script type="text/javascript">
						function keydown() {
							if (event.keyCode==13) {
								var searchWord = $("#searchWord").val();
								location.href="friendSearchPage.do?searchWord="+searchWord;
							}
						}
					</script>
				</div>
			</div>
		</div>
	<br/>
		<div class="table-responsive col-xs-10 text-center">
			<table class="table">
				<thead>
					<tr>
						<th>이름</th>
						<th>이메일</th>
						<th>생년월일</th>
					</tr>
				</thead>
				<c:choose>
					<c:when test="${friendsInfo != null}">
						<c:forEach var="dto" items="${friendsInfo}">
							<tbody>
								<tr>
									<td>${dto.userName}</td>
									<td>${dto.email}</td>
									<td>${dto.birthday}</td>
									<td>
									<c:choose>
										<c:when test="${dto.friendNo == fdto.userNo}">
											<a href="/treegoram/unfollow.do?searchWord=${fdto.searchWord}&friendNo=${dto.userNo}&userNo=${fdto.userNo}">
										<button type="button" class="btn btn-success" style="width: 100px">Followed</button>
										</a>
										</c:when>
										<c:otherwise>
											<a href="/treegoram/follow.do?searchWord=${fdto.searchWord}&friendNo=${dto.userNo}&userNo=${fdto.userNo}">
										<button type="button" class="btn btn-primary" style="width: 100px">Follow</button>
										</a>
										</c:otherwise>
									</c:choose>
									</td>
								</tr>
							</tbody>
						</c:forEach>
					</c:when>
				</c:choose>
			</table>
		</div>
	</div>
</body>
</html>
